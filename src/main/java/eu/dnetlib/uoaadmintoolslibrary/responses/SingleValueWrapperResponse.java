package eu.dnetlib.uoaadmintoolslibrary.responses;

public class SingleValueWrapperResponse<T> {
    private T value = null;

    public SingleValueWrapperResponse() { }
    public SingleValueWrapperResponse(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}

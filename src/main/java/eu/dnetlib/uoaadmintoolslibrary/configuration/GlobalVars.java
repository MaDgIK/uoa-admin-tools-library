package eu.dnetlib.uoaadmintoolslibrary.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Date;

@ConfigurationProperties("admintoolslibrary.globalVars")
public class GlobalVars {
    public static Date date = new Date();
    private Date buildDate;
    private String version;

    public String getBuildDate() {
        if(buildDate == null) {
            return null;
        }
        return buildDate.toString();
    }

    public void setBuildDate(Date buildDate) {
        this.buildDate = buildDate;
    }

    public static Date getDate() {
        return date;
    }

    public static void setDate(Date date) {
        GlobalVars.date = date;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}

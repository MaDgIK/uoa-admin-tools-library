package eu.dnetlib.uoaadmintoolslibrary.controllers;

import eu.dnetlib.uoaadmintoolslibrary.services.DivHelpContentService;

import eu.dnetlib.uoaadmintoolslibrary.services.PortalService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class DivHelpContentController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private DivHelpContentService divHelpContentService;

    @Autowired
    private PortalService portalService;

//    // not used by portals
//    @RequestMapping(value = "/divhelpcontent", method = RequestMethod.GET)
//    public List<DivHelpContentResponse> getDivHelpContents(@RequestParam(value = "portal", required = false) String pid,
//                                                           @RequestParam(value = "page", required = false) String page_route,
//                                                           @RequestParam(value = "divId", required = false) String divIdId,
//                                                           @RequestParam(required = false) String active) {
//        return divHelpContentService.getDivHelpContents(pid, page_route, divIdId, active);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/divhelpcontent/{id}", method = RequestMethod.GET)
//    public DivHelpContent getDivHelpContent(@PathVariable(value = "id") String id) {
//        return divHelpContentService.getDivHelpContent(id);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/divhelpcontent", method = RequestMethod.POST)
//    public DivHelpContent insertOrUpdateDivHelpContent(@RequestBody DivHelpContent divHelpContent) {
//
//        return divHelpContentService.insertOrUpdateDivHelpContent(divHelpContent);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/divhelpcontent/delete", method = RequestMethod.POST)
//    public Boolean deleteDivHelpContents(@RequestBody List<String> divHelpContents) throws Exception {
//        return divHelpContentService.deleteDivHelpContents(divHelpContents);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/divhelpcontent/toggle", method = RequestMethod.POST)
//    public List<String> toggleDivHelpContent(@RequestBody List<String> divHelpContents, @RequestParam String status) throws Exception {
//        return divHelpContentService.toggleDivHelpContent(divHelpContents, status);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/divhelpcontent/{id}", method = RequestMethod.DELETE)
//    public void deleteDivHelpContent(@PathVariable(value = "id") String id) {
//        divHelpContentService.deleteDivHelpContent(id);
//    }

}

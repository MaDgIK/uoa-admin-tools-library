package eu.dnetlib.uoaadmintoolslibrary.controllers;

import eu.dnetlib.uoaadmintoolslibrary.entities.Page;
import eu.dnetlib.uoaadmintoolslibrary.entities.plugin.Plugin;
import eu.dnetlib.uoaadmintoolslibrary.entities.plugin.PluginTemplate;
import eu.dnetlib.uoaadmintoolslibrary.services.PageService;
import eu.dnetlib.uoaadmintoolslibrary.services.PluginService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class PluginController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private PluginService pluginService;
    @Autowired
    private PageService pageService;
    // used WITHOUT ANY PARAMS
    @RequestMapping(value = "/plugins", method = RequestMethod.GET)
    public List<Plugin> getPlugins() {
        return pluginService.getPlugins(null, null, null);
    }

    @RequestMapping(value = "/plugins/template/{id}", method = RequestMethod.GET)
    public List<Plugin> getPluginsByTemplate(@PathVariable(value = "id") String id) {
        return pluginService.getPluginsByTemplateId(id);
    }

    @RequestMapping(value = "/pluginTemplates", method = RequestMethod.GET)
    public List<PluginTemplate> getPluginTemplates(/*@RequestParam(required = false) String page,
                                              @RequestParam(required = false) String name,
                                              @RequestParam(value="portal", required = false) String pid*/) {
        return pluginService.getPluginTemplates(null, null, null);
    }
    @RequestMapping(value = "/pluginTemplates/page/{pageId}", method = RequestMethod.GET)
    public List<PluginTemplate> getPluginTemplatesByPage(@PathVariable(value = "pageId") String pageId) {
        return pluginService.getPluginTemplates(pageId, null, null);
    }
    @RequestMapping(value = "/pluginTemplates/{id}", method = RequestMethod.GET)
    public PluginTemplate getPluginTemplateById(@PathVariable(value = "id") String id) {
        return pluginService.getPluginTemplateById(id);
    }
    @RequestMapping(value = "/plugin/{id}", method = RequestMethod.GET)
    public Plugin getPluginById(@PathVariable(value = "id") String id) {
        return pluginService.getPluginById(id);
    }

    // used
    @PreAuthorize("hasAnyAuthority(" +
            "@AuthorizationService.PORTAL_ADMIN, " +
            "@AuthorizationService.curator('community'))")
    @RequestMapping(value = "/pluginTemplate/save", method = RequestMethod.POST)
    public PluginTemplate insertTemplate(@RequestBody PluginTemplate pluginTemplate ) {
        return pluginService.savePluginTemplate(pluginTemplate);
    }
    @PreAuthorize("hasAnyAuthority(" +
            "@AuthorizationService.PORTAL_ADMIN, " +
            "@AuthorizationService.curator('community'))")
    @RequestMapping(value = "/pluginTemplate/save/order/{position}", method = RequestMethod.POST)
    public PluginTemplate updatePluginTemplateOrder(@RequestBody PluginTemplate pluginTemplate, @PathVariable(value = "position") Integer position ) {
        PluginTemplate pt = pluginService.getPluginTemplateById(pluginTemplate.getId());
        pt.setOrder(pt.getOrder() != null ? pt.getOrder() + position: 0 );
        return pluginService.savePluginTemplate(pt);
    }
    @PreAuthorize("hasAnyAuthority(" +
            "@AuthorizationService.PORTAL_ADMIN, " +
            "@AuthorizationService.curator('community')," +
            "@AuthorizationService.manager('community', #pid))")
        @RequestMapping(value = "/community/{pid}/plugin/save/order/{position}", method = RequestMethod.POST)
    public Plugin updatePluginOrder(@PathVariable String pid,@RequestBody Plugin plugin, @PathVariable(value = "position") Integer position ) {
        Plugin pl = pluginService.getPluginById(plugin.getId());
        pl.setOrder(pl.getOrder() != null ? pl.getOrder() + position: 0 );
        return pluginService.savePlugin(pl);
    }
    @PreAuthorize("hasAnyAuthority(" +
            "@AuthorizationService.PORTAL_ADMIN, " +
            "@AuthorizationService.curator('community')," +
            "@AuthorizationService.manager('community', #pid))")
    @RequestMapping(value = "/community/{pid}/plugin/save", method = RequestMethod.POST)
    public Plugin savePlugin(@PathVariable String pid, @RequestBody Plugin plugin ) {
        return pluginService.savePlugin(plugin);
    }

    @PreAuthorize("hasAnyAuthority(" +
            "@AuthorizationService.PORTAL_ADMIN, " +
            "@AuthorizationService.curator('community'))")
    @RequestMapping(value = "/community/{pid}/plugin/status/{id}", method = RequestMethod.POST)
    public Plugin updatePluginStatus(@PathVariable String pid, @PathVariable(value = "id") String id, @RequestBody String status ) {
        return pluginService.updatePluginStatus(id,status);
    }

    @RequestMapping(value = "/pluginTemplate/page/count", method = RequestMethod.GET)
    public Map<String, Integer> countPluginTemplatesForPages() {
        Map<String, Integer> mapCount = new HashMap<String, Integer>();

        List<Page> pages = pageService.getAllPages(null, null, null);
        for(Page page : pages){
            List<PluginTemplate> plugin = pluginService.getPluginTemplatesByPage( page.getId());
            if (plugin == null) {
                mapCount.put(page.getId(), 0);
            } else {
                mapCount.put(page.getId(), plugin.size());
            }
        }
        return mapCount;
    }

    // not used by portals
    @PreAuthorize("hasAnyAuthority(" +
            "@AuthorizationService.PORTAL_ADMIN, " +
            "@AuthorizationService.curator('community'))")
    @RequestMapping(value = "/pluginTemplate/{id}", method = RequestMethod.DELETE)
    public void deletePluginTemplate(@PathVariable(value = "id") String id) {
        pluginService.deletePluginTemplate(id);
    }
    // not used by portals
    @PreAuthorize("hasAnyAuthority(" +
            "@AuthorizationService.PORTAL_ADMIN, " +
            "@AuthorizationService.curator('community'))")
    @RequestMapping(value = "/plugin/{id}", method = RequestMethod.DELETE)
    public void deletePlugin(@PathVariable(value = "id") String id) {
        pluginService.deletePlugin(id);
    }

    @RequestMapping(value = "/plugin/template/count", method = RequestMethod.GET)
    public Map<String, Integer> countPluginsByTemplate() {
        Map<String, Integer> mapCount = new HashMap<String, Integer>();

        List<PluginTemplate> templates = pluginService.getPluginTemplates(null,null,null);
        for(PluginTemplate template: templates){
            List<Plugin> plugins = pluginService.getPluginsByTemplateId(template.getId());
            if (plugins == null) {
                mapCount.put(template.getId(), 0);
            } else {
                mapCount.put(template.getId(), plugins.size());
            }
        }
        return mapCount;
    }
   }

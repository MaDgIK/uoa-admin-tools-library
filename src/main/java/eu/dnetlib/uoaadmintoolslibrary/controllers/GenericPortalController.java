package eu.dnetlib.uoaadmintoolslibrary.controllers;

import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.PortalResponse;
import eu.dnetlib.uoaadmintoolslibrary.services.PortalService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// not used by portals
@RestController
@RequestMapping("/portal")
@CrossOrigin(origins = "*")
public class GenericPortalController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private PortalService portalService;

    @RequestMapping(value = "/{pid}/type", method = RequestMethod.GET)
    public String getPortalType(@PathVariable String pid) {
        Portal portal = portalService.getPortal(pid);
        if (portal == null) {
            return null;
        } else {
            return portal.getType();
        }
    }

    @RequestMapping(value = {""}, method = RequestMethod.GET)
    public List<Portal> getAllPortals() {
        List<Portal> portals = portalService.getAllPortals();

        return portals;
    }

    @RequestMapping(value = {"/full"}, method = RequestMethod.GET)
    public List<PortalResponse> getAllPortalsFull() {
        return portalService.getAllPortalsFull();
    }

//    @PreAuthorize("isAuthenticated()")
//    @RequestMapping(value = {"/my-portals"}, method = RequestMethod.GET)
//    public List<Portal> getMyPortals(
//            //@RequestHeader("X-XSRF-TOKEN") String token
//
//            ) {
////        log.debug(token);
////        UserInfo userInfo = utils.getUserInfo(token);
////        log.debug(userInfo);
////        if(userInfo != null) {
////            List<String> roles = userInfo.getRoles();
////            for (String role : roles) {
////                log.debug(role);
////            }
////        }
//
//        List<GrantedAuthority> authorities = null;
//        Authentication authentication = (Authentication) SecurityContextHolder.getContext().getAuthentication();
//        if(authentication != null) {
//           authorities = (List<GrantedAuthority>) authentication.getAuthorities();
//        }
//        log.debug(authorities);
//
////        for(GrantedAuthority authority : authorities) {
////            authorizationService.
////        }
//        List<Portal> portals = portalService.getAllPortals();
//        for(Portal portal : portals) {
//            if(authorizationService..manager(portal.getType(), portal.getPid())) {
//
//            }
//        }
//
//        List<Portal> myPortals = new ArrayList<>();
//        // Get roles and for every role, find portalType and add portals
//        List<String> portalTypes = new ArrayList<>();
//        portalTypes.add("connect");
//        portalTypes.add("community");
//        portalTypes.add("explore");
//        for(String portalType : portalTypes) {
//            myPortals.addAll(portalService.getAllPortalsByType(portalType));
//            log.debug("Num of portals now: "+myPortals.size());
//        }
//
//        return myPortals;
//    }
}
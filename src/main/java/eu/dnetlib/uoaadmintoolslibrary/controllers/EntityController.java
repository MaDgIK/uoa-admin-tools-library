package eu.dnetlib.uoaadmintoolslibrary.controllers;

import eu.dnetlib.uoaadmintoolslibrary.entities.Entity;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.PortalEntity;
import eu.dnetlib.uoaadmintoolslibrary.services.EntityService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class EntityController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private EntityService entityService;

    // used
    @RequestMapping(value = "/entity", method = RequestMethod.GET)
    public List<Entity> getAllEntities() {
        return entityService.getAllEntities();
    }

    // used
    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/entity/save", method = RequestMethod.POST)
    public PortalEntity insertEntity(@RequestBody Entity entity) {
        return entityService.insertEntity(entity);
    }

    // used
    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/entity/update", method = RequestMethod.POST)
    public PortalEntity updateEntity(@RequestBody PortalEntity portalEntity) {
        return entityService.updateEntity(portalEntity);
    }

    // used
    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/entity/delete", method = RequestMethod.POST)
    public Boolean deleteEntities(@RequestBody List<String> entities) throws Exception {
        return entityService.deleteEntities(entities);
    }

//    // not used by portals
//    @RequestMapping(value = "/entity", method = RequestMethod.DELETE)
//    public void deleteAllEntities() {
//        entityService.deleteAllEntities();
//    }
//
//    // not used by portals    @RequestMapping(value = "/entity", method = RequestMethod.POST)
//    public Entity insertOrUpdateEntity(@RequestBody Entity entity) {
//        return entityService.insertOrUpdateEntity(entity);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/entity/{id}", method = RequestMethod.GET)
//    public Entity getEntity(@PathVariable(value = "id") String id) {
//        return entityService.getEntity(id);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/entity/{id}", method = RequestMethod.DELETE)
//    public void deleteEntity(@PathVariable(value = "id") String id) {
//        entityService.deleteEntity(id);
//    }
}
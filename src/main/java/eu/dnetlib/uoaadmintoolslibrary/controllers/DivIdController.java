package eu.dnetlib.uoaadmintoolslibrary.controllers;

import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.DivIdResponse;
import eu.dnetlib.uoaadmintoolslibrary.services.DivIdService;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;

@RestController
@CrossOrigin(origins = "*")
public class DivIdController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private DivIdService divIdService;

//    // not used by portals
//    @RequestMapping(value = "/div", method = RequestMethod.GET)
//    public List<DivId> getDivIds(@RequestParam(required = false) String page,
//                                 @RequestParam(required = false) String name,
//                                 @RequestParam(value = "portal", required = false) String pid) {
//        return divIdService.getDivIds(page, name, pid);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/div/{id}", method = RequestMethod.GET)
//    public DivId getDivId(@PathVariable(value = "id") String id) {
//        return divIdService.getDivId(id);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/div/{id}/full", method = RequestMethod.GET)
//    public DivIdResponse getDivIdFull(@PathVariable(value = "id") String id) {
//        return divIdService.getDivIdFull(id);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/div", method = RequestMethod.DELETE)
//    public void deleteAllDivIds() {
//        divIdService.deleteAllDivIds();
//    }

    // used WITHOUT ANY PARAMS
    @RequestMapping(value = "/div/full", method = RequestMethod.GET)
    public List<DivIdResponse> getDivIdsFull(@RequestParam(required = false) String page,
                                             @RequestParam(required = false) String name,
                                             @RequestParam(value="portal", required = false) String pid) {
        return divIdService.getDivIdsFull(page, name, pid);
    }

    // used
    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/div/save", method = RequestMethod.POST)
    public DivIdResponse insertDivId(@RequestBody DivIdResponse divIdResponse) {
        return divIdService.insertDivId(divIdResponse);
    }

    // used
    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/div/update", method = RequestMethod.POST)
    public DivIdResponse updateDivId(@RequestBody DivIdResponse divIdResponse) {
        return divIdService.updateDivId(divIdResponse);
    }

    // used
    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/div/delete", method = RequestMethod.POST)
    public Boolean deleteDivIds(@RequestBody List<String> divIds) throws Exception {
        return divIdService.deleteDivIds(divIds);
    }

//    // not used by portals
//    @RequestMapping(value = "/div/{id}", method = RequestMethod.DELETE)
//    public void deleteDivId(@PathVariable(value = "id") String id) {
//        divIdService.deleteDivId(id);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/div/pages", method = RequestMethod.GET)
//    public Set<String> getDivIdsPages(@RequestParam(value="portal", required = false) String pid) {
//        return divIdService.getDivIdsPages(pid);
//    }

}

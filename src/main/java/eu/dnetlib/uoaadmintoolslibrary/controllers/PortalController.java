package eu.dnetlib.uoaadmintoolslibrary.controllers;

import eu.dnetlib.uoaadmintoolslibrary.entities.*;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.*;
import eu.dnetlib.uoaadmintoolslibrary.services.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "{portalType}")
@CrossOrigin(origins = "*")
public class PortalController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private PortalService portalService;

//    // not used by portals
//    @RequestMapping(value = {"/"}, method = RequestMethod.POST)
//    public Portal insertOrUpdatePortal(@PathVariable PortalType portalType, @RequestBody Portal portal) {
//        return portalService.insertOrUpdatePortal(portal);
//    }
//
//    // not used by portals
//    @RequestMapping(value = {"/{id}"}, method = RequestMethod.DELETE)
//    public void deletePortal(@PathVariable PortalType portalType, @PathVariable(value = "id") String id) {
//        portalService.deletePortalId(id);
//    }

    // used
    @RequestMapping(value = {"/{pid}"}, method = RequestMethod.GET)
    public Portal getPortal(@PathVariable PortalType portalType, @PathVariable(value = "pid") String pid) {
        log.debug("PID: "+ pid);
        return portalService.getPortal(pid);
    }

    // used
    @RequestMapping(value = {"/{pid}/full"}, method = RequestMethod.GET)
    public PortalResponse getPortalFull(@PathVariable PortalType portalType, @PathVariable(value = "pid") String pid) {
        return portalService.getPortalFull(pid);
    }

    // used
    @RequestMapping(value = {"/{pid}/divhelpcontent/grouped"}, method = RequestMethod.GET)
    public Map<String, List<DivHelpContentResponse>> getDivHelpContentsByPosition(@PathVariable PortalType portalType,
                                                                                  @PathVariable(value = "pid") String pid,
                                                                                  @RequestParam(required=true) String page,
                                                                                  @RequestParam(required=true) String active) {
        return portalService.getDivHelpContentsByPosition(pid, page, active);
    }

    // used
    @RequestMapping(value = {"/{pid}/pagehelpcontent/grouped"}, method = RequestMethod.GET)
    public Map<String, List<PageHelpContentResponse>> getPageHelpContentsByPosition(@PathVariable PortalType portalType,
                                                                                    @PathVariable(value = "pid") String pid,
                                                                                    @RequestParam(required=true) String page,
                                                                                    @RequestParam(required=true) String active) {
        return portalService.getPageHelpContentsByPosition(pid, page, active);
    }
}
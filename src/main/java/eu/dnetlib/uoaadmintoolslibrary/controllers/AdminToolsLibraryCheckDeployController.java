package eu.dnetlib.uoaadmintoolslibrary.controllers;

import eu.dnetlib.uoaadmintoolslibrary.configuration.GlobalVars;
import eu.dnetlib.uoaadmintoolslibrary.configuration.properties.GoogleConfig;
import eu.dnetlib.uoaadmintoolslibrary.configuration.properties.MailConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/admin-tools-library")
public class AdminToolsLibraryCheckDeployController {
    @Autowired
    private MailConfig mailConfig;

    @Autowired
    private GoogleConfig googleConfig;

    @Autowired
    private GlobalVars globalVars;

    private final Logger log = LogManager.getLogger(this.getClass());

    @RequestMapping(value = {"", "/health_check"}, method = RequestMethod.GET)
    public String hello() {
        log.debug("Hello from uoa-admin-tools-library!");
        return "Hello from uoa-admin-tools-library!";
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/health_check/advanced", method = RequestMethod.GET)
    public Map<String, String> checkEverything() {
        Map<String, String> response = new HashMap<>();
        response.put("admintoolslibrary.mail.auth",mailConfig.getAuth());
        response.put("admintoolslibrary.mail.sslProtocols",mailConfig.getSslProtocols());
        response.put("admintoolslibrary.mail.from", mailConfig.getFrom());
        response.put("admintoolslibrary.mail.host", mailConfig.getHost());
        response.put("admintoolslibrary.mail.port", mailConfig.getPort());
        response.put("admintoolslibrary.mail.username", mailConfig.getUsername() == null ? null : "[unexposed value]");
        response.put("admintoolslibrary.mail.password", mailConfig.getPassword() == null ? null : "[unexposed value]");
        response.put("admintoolslibrary.google.secret", googleConfig.getSecret() == null ? null : "[unexposed value]");

        if(GlobalVars.date != null) {
            response.put("Date of deploy", GlobalVars.date.toString());
        }
        if(globalVars.getBuildDate() != null) {
            response.put("Date of build", globalVars.getBuildDate());
        }
        if (globalVars.getVersion() != null) {
            response.put("Version", globalVars.getVersion());
        }
        return response;
    }
}


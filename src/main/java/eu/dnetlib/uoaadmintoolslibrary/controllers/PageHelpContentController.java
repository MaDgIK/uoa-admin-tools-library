package eu.dnetlib.uoaadmintoolslibrary.controllers;

import eu.dnetlib.uoaadmintoolslibrary.services.PageHelpContentService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
public class PageHelpContentController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private PageHelpContentService pageHelpContentService;

//    // not used by portals
//    @RequestMapping(value = "/pagehelpcontent", method = RequestMethod.GET)
//    public List<PageHelpContentResponse> getPageHelpContents(@RequestParam(value = "portal", required = false) String pid,
//                                                             @RequestParam(required=false) String portalType,
//                                                             @RequestParam(required=false) String page,
//                                                             @RequestParam(required=false) String position,
//                                                             @RequestParam(required=false) String active,
//                                                             @RequestParam(required=false) String before) {
//        return pageHelpContentService.getPageHelpContents(pid, portalType, page, position, active, before);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/pagehelpcontent", method = RequestMethod.DELETE)
//    public void deleteAllPageHelpContents() {
//        pageHelpContentService.deleteAllPageHelpContents();
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/pagehelpcontent/save", method = RequestMethod.POST)
//    public PageHelpContent insertPageHelpContent(@RequestBody PageHelpContent pageHelpContent) {
//        return pageHelpContentService.insertPageHelpContent(pageHelpContent);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/pagehelpcontent/update", method = RequestMethod.POST)
//    public PageHelpContent updatePageHelpContent(@RequestBody PageHelpContent pageHelpContent) {
//        return pageHelpContentService.updatePageHelpContent(pageHelpContent);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/pagehelpcontent/{id}", method = RequestMethod.GET)
//    public PageHelpContent getPageHelpContent(@PathVariable(value = "id") String id) {
//        return pageHelpContentService.getPageHelpContent(id);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/pagehelpcontent/toggle", method = RequestMethod.POST)
//    public List<String> togglePageHelpContent(@RequestBody List<String> pageHelpContents, @RequestParam String status) throws Exception {
//        return pageHelpContentService.togglePageHelpContent(pageHelpContents, status);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/pagehelpcontent/{id}", method = RequestMethod.DELETE)
//    public void deletePageHelpContent(@PathVariable(value = "id") String id) {
//        pageHelpContentService.deletePageHelpContent(id);
//    }
//
//    // not used by portals
//    @RequestMapping(value = "/pagehelpcontent/delete", method = RequestMethod.POST)
//    public Boolean deletePageHelpContents(@RequestBody List<String> pageHelpContents) throws Exception {
//        return pageHelpContentService.deletePageHelpContents(pageHelpContents);
//    }
}

package eu.dnetlib.uoaadmintoolslibrary.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MismatchingContentException extends RuntimeException {
    public MismatchingContentException(String message){
        super(message);
    }
}

package eu.dnetlib.uoaadmintoolslibrary.handlers.utils;

import eu.dnetlib.uoaauthorizationlibrary.security.AuthorizationService;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RolesUtils {

    @Autowired
    private AuthorizationService authorizationService;

    private final Logger log = LogManager.getLogger(this.getClass());

    public List<String> getRoles() {
        return authorizationService.getRoles();
    }

    public String getEmail() {
        return authorizationService.getEmail();
    }

    public String getAaiId() {
        return authorizationService.getAaiId();
    }

    public boolean isPortalAdmin() {
        return this.authorizationService.getRoles().contains(authorizationService.PORTAL_ADMIN);
    }

    public boolean isCurator(String type) {
        return this.authorizationService.getRoles().contains(authorizationService.curator(type));
    }

    public boolean isManager(String type, String id) {
        return this.authorizationService.getRoles().contains(authorizationService.manager(type, id));
    }

    public boolean isMember(String type, String id) {
        return this.authorizationService.getRoles().contains(authorizationService.member(type, id));
    }

    public boolean isLoggedIn() {
        return this.authorizationService.getAaiId() != null;
    }

    public boolean hasUpdateAuthority(String type, String id) {
        return isPortalAdmin() || isCurator(type) || isManager(type, id);
    }

    public boolean hasCreateAndDeleteAuthority(String type) {
        return isPortalAdmin() || isCurator(type);
    }
}

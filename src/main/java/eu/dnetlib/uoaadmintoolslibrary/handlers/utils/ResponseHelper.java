package eu.dnetlib.uoaadmintoolslibrary.handlers.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.util.DigestUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class ResponseHelper {

    public static String generateETag(String content) {
        // Generate a unique hash based on the content
        String hash = DigestUtils.md5DigestAsHex(content.getBytes());
        return "\"" + hash + "\""; // ETag format: "hash_value"
    }

    public  static HttpHeaders getCacheHeadersWithTag(String response, Date date){
        HttpHeaders headers = new HttpHeaders();
        headers.setETag(ResponseHelper.generateETag(response));
        headers.setCacheControl("max-age=3600");
        if(date != null) {
            headers.setLastModified(date.getTime());
        }
        Date now = new Date();
        headers.setExpires(now.getTime() + 3600000);
        headers.setExpires(now.getTime() + 7200000);
        return headers;
    }
    public  static void getCacheHeadersWithTag(String response, HttpServletResponse HSResponse, HttpServletRequest request){
        String tag = ResponseHelper.generateETag(response.toString());
        String ifMatchHeader = request.getHeader("If-Match");
        if (ifMatchHeader != null && ifMatchHeader.equals(tag)) {
            HSResponse.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
        } else {

            Date now = new Date();
            now.setTime(now.getTime() + 3600000);
            HSResponse.setHeader("ETag",tag);
            HSResponse.setHeader("Cache-Control","no-cache, max-age=3600");
            HSResponse.setHeader("Expires",ResponseHelper.getvalidHeaderDateFormat(now));
            //TODO need to set proper date
//            HSResponse.setHeader("Last-Modified",  ResponseHelper.getvalidHeaderDateFormat(new Date(1640995200000L)));
        }
    }
    public  static void getCacheHeadersWithTag(  String tag,  HttpServletResponse HSResponse){
        Date now = new Date();
        now.setTime(now.getTime() + 3600000);
        HSResponse.setHeader("ETag",tag);
        HSResponse.setHeader("Cache-Control","no-cache, max-age=3600");
        HSResponse.setHeader("Expires",ResponseHelper.getvalidHeaderDateFormat(now));
        HSResponse.setHeader("Last-Modified",  ResponseHelper.getvalidHeaderDateFormat(new Date(1640995200000L)));

    }
    public static String getvalidHeaderDateFormat(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(date);

    }
}

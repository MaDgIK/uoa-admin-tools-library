package eu.dnetlib.uoaadmintoolslibrary.handlers;

import eu.dnetlib.uoaadmintoolslibrary.responses.ExceptionResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AdminToolsLibraryExceptionsHandler {
    private final Logger log = LogManager.getLogger(this.getClass());

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ExceptionResponse> invalidInput(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Validation Error");
        response.setErrorMessage("Invalid inputs.");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.BAD_REQUEST);
        log.error("invalidInput exception");

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MismatchingContentException.class)
    public ResponseEntity<ExceptionResponse> mismatchingContentException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Conflicting content given");
        response.setErrorMessage(ex.getMessage());
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.BAD_REQUEST);
        log.error("mismatchingContent exception: " + response.getErrorCode()+ " "+response.getErrorMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ContentNotFoundException.class)
    public ResponseEntity<ExceptionResponse> contentNotFoundException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("No content found");
        response.setErrorMessage(ex.getMessage());
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.NOT_FOUND);
        log.error("contentNotFound exception: " + response.getErrorCode()+ " - "+response.getErrorMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<ExceptionResponse> nullPointerException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Null pointer Exception");
        response.setErrorMessage("Null pointer Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.BAD_REQUEST);
        log.error("nullPointerException exception" + response.getErrorCode()+ " - "+response.getErrorMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidReCaptchaException.class)
    public ResponseEntity<ExceptionResponse> invalidReCaptchaException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Invalid ReCaptcha Exception");
        response.setErrorMessage("Invalid ReCaptcha Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.BAD_REQUEST);
        log.error("invalidReCaptchaException exception");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(ChangeSetPersister.NotFoundException.class)
    public ResponseEntity<ExceptionResponse> notFoundException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Not found Exception");
        response.setErrorMessage("Not found Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.NOT_FOUND);
        log.error("notFoundException exception : "+ ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<ExceptionResponse> unauthorizedException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Unauthorized Exception");
        response.setErrorMessage("Unauthorized Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.UNAUTHORIZED);
        log.error("Unauthorized exception: "+ ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<ExceptionResponse> forbiddenException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Forbidden Exception");
        response.setErrorMessage("Forbidden Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.FORBIDDEN);
        log.error("forbiddenException exception: "+ ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(DuplicateKeyException.class)
    public ResponseEntity<ExceptionResponse> duplicateKeyException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("DuplicateKey Exception");
        response.setErrorMessage("DuplicateKey Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        log.error("duplicateKeyException exception: "+ ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

package eu.dnetlib.uoaadmintoolslibrary.dao;

import java.util.List;

import eu.dnetlib.uoaadmintoolslibrary.entities.PageHelpContent;

public interface PageHelpContentDAO {
    List<PageHelpContent> findAll();

    List<PageHelpContent> findByPortalAndPlacementAndIsActiveAndIsPriorToOrderByOrderAsc(String portalId, String position, boolean isActive, boolean isPriorTo);
    List<PageHelpContent> findByPortalAndPlacementAndIsActiveOrderByOrderAsc(String portalId, String position, boolean isActive);
    List<PageHelpContent> findByPortalAndPlacementAndIsPriorToOrderByOrderAsc(String portalId, String position, boolean isPriorTo);
    List<PageHelpContent> findByPortalAndIsActiveAndIsPriorToOrderByPlacementAscOrderAsc(String portalId, boolean isActive, boolean isPriorTo);
    List<PageHelpContent> findByPlacementAndIsActiveAndIsPriorToOrderByOrderAsc(String position, boolean isActive, boolean isPriorTo);
    List<PageHelpContent> findByPortalAndPlacementOrderByOrderAsc(String portalId, String postion);
    List<PageHelpContent> findByPortalAndIsActiveOrderByPlacementAscOrderAsc(String portalId, boolean isActive);
    List<PageHelpContent> findByPortalAndIsPriorToOrderByPlacementAscOrderAsc(String portalId, boolean isPriorTo);
    List<PageHelpContent> findByPlacementAndIsActiveOrderByOrderAsc(String position, boolean isActive);
    List<PageHelpContent> findByPlacementAndIsPriorToOrderByOrderAsc(String position, boolean isPriorTo);
    List<PageHelpContent> findByIsActiveAndIsPriorToOrderByPlacementAscOrderAsc(boolean isActive, boolean isPriorTo);
    List<PageHelpContent> findByPortalOrderByPlacementAscOrderAsc(String portalId);
    List<PageHelpContent> findByPortalAndPageOrderByPlacementAscOrderAsc(String portalId, String page);
    List<PageHelpContent> findByPlacementOrderByOrderAsc(String postion);
    List<PageHelpContent> findByIsActiveOrderByPlacementAscOrderAsc(boolean isActive);
    List<PageHelpContent> findByIsPriorToOrderByPlacementAscOrderAsc(boolean isPriorTo);
    List<PageHelpContent> findAllByOrderByPlacementAscOrderAsc();

    //List<PageHelpContent> findByPortalAndPlacementAndIsActiveAndIsPriorToOrderByOrderAsc(String portalId, String postion, boolean isActive, boolean isBefore);


    PageHelpContent findById(String Id);

    PageHelpContent findByIdOrderByOrder(String Id);

    PageHelpContent save(PageHelpContent pageHelpContent);

    void deleteAll();

    void delete(String id);
}
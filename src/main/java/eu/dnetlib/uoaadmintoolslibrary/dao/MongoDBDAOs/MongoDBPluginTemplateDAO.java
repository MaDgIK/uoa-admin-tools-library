package eu.dnetlib.uoaadmintoolslibrary.dao.MongoDBDAOs;

import eu.dnetlib.uoaadmintoolslibrary.dao.DivIdDAO;
import eu.dnetlib.uoaadmintoolslibrary.dao.PluginTemplateDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.DivId;
import eu.dnetlib.uoaadmintoolslibrary.entities.plugin.PluginTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface MongoDBPluginTemplateDAO extends PluginTemplateDAO, MongoRepository<PluginTemplate, String> {
    PluginTemplate findById(String Id);
    PluginTemplate findByCode(String code);
    List<PluginTemplate> findAll();
    @Query("{ pages:  ?0   }")
    List<PluginTemplate> findByPages(String page);
    PluginTemplate save(PluginTemplate pluginTemplate);

    void deleteAll();

    void delete(String id);
}
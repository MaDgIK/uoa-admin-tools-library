package eu.dnetlib.uoaadmintoolslibrary.dao.MongoDBDAOs;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

import eu.dnetlib.uoaadmintoolslibrary.dao.DivHelpContentDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.DivHelpContent;

public interface MongoDBDivHelpContentDAO extends DivHelpContentDAO, MongoRepository<DivHelpContent, String> {
    List<DivHelpContent> findAll();

    List<DivHelpContent> findByPortal(String portalId);
    List<DivHelpContent> findByDivId(String divId);
    List<DivHelpContent> findByIsActive(boolean isActive);
    List<DivHelpContent> findByPortalAndDivId(String portalId, String divId);
    List<DivHelpContent> findByPortalAndIsActive(String portalId, boolean isActive);
    List<DivHelpContent> findByDivIdAndIsActive(String divId, boolean isActive);
    List<DivHelpContent> findByPortalAndDivIdAndIsActive(String portalId, String divId, boolean isActive);
//    List<DivHelpContent> findByPortalAndPage(String portalId, String page);

    DivHelpContent findById(String Id);

    DivHelpContent save(DivHelpContent divHelpContent);

    void deleteAll();

    void delete(String id);
}

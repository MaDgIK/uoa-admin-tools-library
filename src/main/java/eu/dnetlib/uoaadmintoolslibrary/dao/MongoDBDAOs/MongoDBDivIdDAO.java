package eu.dnetlib.uoaadmintoolslibrary.dao.MongoDBDAOs;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

import eu.dnetlib.uoaadmintoolslibrary.dao.DivIdDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.DivId;

public interface MongoDBDivIdDAO extends DivIdDAO, MongoRepository<DivId, String> {
    DivId findByPagesContainingAndNameAndPortalType(String page, String name, String portalType);
    DivId findByPagesContainingAndName(String page, String name);
    DivId findByNameAndPortalType(String name, String portalType);
    DivId findById(String Id);

    List<DivId> findAll();
    List<DivId> findByPagesContaining(String page);
    List<DivId> findByName(String name);
    List<DivId> findByPortalType(String portalType);
    List<DivId> findByPagesContainingAndPortalType(String page, String portalType);

    DivId save(DivId divId);

    void deleteAll();

    void delete(String id);
}
package eu.dnetlib.uoaadmintoolslibrary.dao.MongoDBDAOs;

import eu.dnetlib.uoaadmintoolslibrary.dao.EntityDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.Entity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoDBEntityDAO extends EntityDAO, MongoRepository<Entity, String> {
    List<Entity> findAll();

    Entity findById(String Id);

    Entity findByPid(String Pid);

    Entity findByName(String name);

    Entity save(Entity entity);

    void deleteAll();

    void delete(String id);
}

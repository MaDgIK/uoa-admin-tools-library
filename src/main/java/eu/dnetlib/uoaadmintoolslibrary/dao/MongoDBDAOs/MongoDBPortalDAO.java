package eu.dnetlib.uoaadmintoolslibrary.dao.MongoDBDAOs;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

import eu.dnetlib.uoaadmintoolslibrary.dao.PortalDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;

public interface MongoDBPortalDAO extends PortalDAO, MongoRepository<Portal, String> {
    List<Portal> findAll();
    List<Portal> findByType(String Type);

    Portal findById(String Id);

    Portal findByPid(String Pid);

    Portal findByName(String Name);

    Portal save(Portal portal);

    void deleteAll();

    void delete(String id);
}

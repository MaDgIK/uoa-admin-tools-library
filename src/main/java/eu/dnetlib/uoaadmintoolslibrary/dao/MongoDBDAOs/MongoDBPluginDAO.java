package eu.dnetlib.uoaadmintoolslibrary.dao.MongoDBDAOs;

import eu.dnetlib.uoaadmintoolslibrary.dao.PluginDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.plugin.Plugin;
import eu.dnetlib.uoaadmintoolslibrary.entities.plugin.PluginTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface MongoDBPluginDAO extends PluginDAO, MongoRepository<Plugin, String> {
    Plugin findById(String Id);

    List<Plugin> findAll();
    List<Plugin> findByPage(String page);
    List<Plugin> findByPageAndPid(String page, String pid);
    List<Plugin> findByTemplateId(String templateId);
    Plugin save(Plugin plugin);
    void deleteAll();
    void delete(String id);
}
package eu.dnetlib.uoaadmintoolslibrary.dao;

import java.util.List;

import eu.dnetlib.uoaadmintoolslibrary.entities.Entity;

public interface EntityDAO {
    List<Entity> findAll();

    Entity findById(String Id);

    Entity findByPid(String Pid);

    Entity findByName(String name);

    Entity save(Entity entity);

    void deleteAll();

    void delete(String id);
}
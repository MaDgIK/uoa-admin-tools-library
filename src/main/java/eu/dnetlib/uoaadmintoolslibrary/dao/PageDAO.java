package eu.dnetlib.uoaadmintoolslibrary.dao;

import java.util.List;

import eu.dnetlib.uoaadmintoolslibrary.entities.Page;
import org.springframework.data.mongodb.repository.Query;

public interface PageDAO {
    List<Page> findAll();

    @Query("{'portalType': ?0, 'portalPid': {$in: [?1, null] }}")
    List<Page> findByPortalTypeAndPortalPidOrNull(String portalType, String portalPid);

    List<Page> findByRoute(String route);

    @Query("{'portalType': ?0, 'route': ?1, 'portalPid': {$in: [?2, null] }}")
    Page findByPortalTypeAndRouteAndPortalPidOrNull(String portalType, String route, String portalPid);

    Page findById(String Id);

    Page save(Page page);

    void deleteAll();

    void delete(String id);
}
package eu.dnetlib.uoaadmintoolslibrary.dao;

import java.util.List;

import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;

public interface PortalDAO {
    List<Portal> findAll();
    List<Portal> findByType(String Type);

    Portal findById(String Id);

    Portal findByPid(String Pid);

    Portal findByName(String Name);

    Portal save(Portal portal);

    void deleteAll();

    void delete(String id);
}

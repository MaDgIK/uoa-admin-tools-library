package eu.dnetlib.uoaadmintoolslibrary.dao;

import eu.dnetlib.uoaadmintoolslibrary.entities.plugin.Plugin;

import java.util.List;

public interface PluginDAO {
    Plugin findById(String Id);

    List<Plugin> findAll();
    List<Plugin> findByPage(String page);
    List<Plugin> findByPageAndPid(String page, String pid);
    List<Plugin> findByTemplateId(String templateId);
    Plugin save(Plugin plugin);
    void deleteAll();
    void delete(String id);
}


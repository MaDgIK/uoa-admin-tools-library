package eu.dnetlib.uoaadmintoolslibrary.dao;

import java.util.List;

import eu.dnetlib.uoaadmintoolslibrary.entities.DivId;

public interface DivIdDAO {
    DivId findByPagesContainingAndNameAndPortalType(String page, String name, String portalType);
    DivId findByPagesContainingAndName(String page, String name);
    DivId findByNameAndPortalType(String name, String portalType);
    DivId findById(String Id);

    List<DivId> findAll();
    List<DivId> findByPagesContaining(String page);
    List<DivId> findByName(String name);
    List<DivId> findByPortalType(String portalType);
    List<DivId> findByPagesContainingAndPortalType(String page, String portalType);

    DivId save(DivId divId);

    void deleteAll();

    void delete(String id);
}


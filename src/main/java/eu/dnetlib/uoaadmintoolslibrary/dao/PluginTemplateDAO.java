package eu.dnetlib.uoaadmintoolslibrary.dao;

import eu.dnetlib.uoaadmintoolslibrary.entities.plugin.PluginTemplate;
import java.util.List;

public interface PluginTemplateDAO {
    PluginTemplate findById(String Id);
    List<PluginTemplate> findAll();
    List<PluginTemplate> findByPage(String page);
    PluginTemplate save(PluginTemplate pluginTemplate);

    void deleteAll();

    void delete(String id);
}


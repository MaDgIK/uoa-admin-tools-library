package eu.dnetlib.uoaadmintoolslibrary;

import eu.dnetlib.uoaadmintoolslibrary.configuration.GlobalVars;
import eu.dnetlib.uoaadmintoolslibrary.configuration.properties.GoogleConfig;
import eu.dnetlib.uoaadmintoolslibrary.configuration.properties.MailConfig;
import eu.dnetlib.uoaauthorizationlibrary.configuration.IgnoreSecurityConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableConfigurationProperties({MailConfig.class, GoogleConfig.class, GlobalVars.class})
@ComponentScan(basePackages = { "eu.dnetlib.uoaadmintoolslibrary" })
@Import({IgnoreSecurityConfiguration.class})
public class UoaAdminToolsLibraryConfiguration {}

package eu.dnetlib.uoaadmintoolslibrary.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.Map;

public class Portal {

    @Id
    @JsonProperty("_id")
    private String id;

    private String pid;
    private String name;
    private PortalType type; // explore, connect, community, monitor
    private String piwik;
    private String twitterAccount;
    private Map<String, Boolean> pages;
    private Map<String, Boolean> entities;

    public Portal() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        if(type == null) {
            return null;
        }
        return type.name();
    }

    public void setType(String type) {
        if(type == null) {
            this.type = null;
        } else {
            PortalType pType = PortalType.valueOf(type);
            this.type = pType;
        }
    }

    public String getPiwik() {
        return piwik;
    }

    public void setPiwik(String piwik) {
        this.piwik = piwik;
    }

    public String getTwitterAccount() {
        return twitterAccount;
    }

    public void setTwitterAccount(String twitterAccount) {
        this.twitterAccount = twitterAccount;
    }

    public Map<String, Boolean> getPages() { return pages; }

    public void setPages(Map<String, Boolean> pages) { this.pages = pages; }

    public Map<String, Boolean> getEntities() { return entities; }

    public void setEntities(Map<String, Boolean> entities) {
        this.entities = entities;
    }
}
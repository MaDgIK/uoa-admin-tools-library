package eu.dnetlib.uoaadmintoolslibrary.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.List;

public class DivId {
    @Id
    @JsonProperty("_id")
    private String id;

    private String name;
    private List<String> pages;
    private PortalType portalType; // explore, connect, community, monitor

//    private Boolean connect;
//    private Boolean communities;
//    private Boolean openaire;

    public DivId() {}

    public DivId(DivId divId) {
        setName(divId.getName());
        setPages(divId.getPages());
        setPortalType(divId.getPortalType());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPages() {
        return pages;
    }

    public void setPages(List<String> pages) {
        this.pages = pages;
    }

    public String getPortalType() {
        if(portalType == null) {
            return null;
        }
        return portalType.name();
    }

    public void setPortalType(String portalType) {
        if(portalType == null) {
            this.portalType = null;
        } else {
            PortalType pType = PortalType.valueOf(portalType);
            this.portalType = pType;
        }
    }
}
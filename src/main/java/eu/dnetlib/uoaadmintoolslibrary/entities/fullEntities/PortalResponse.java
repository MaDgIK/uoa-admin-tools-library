package eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.dnetlib.uoaadmintoolslibrary.entities.PortalType;
import org.springframework.data.annotation.Id;

import java.util.List;

import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;

public class PortalResponse {
    @Id
    @JsonProperty("_id")
    private String id;

    private String pid;
    private String name;
    private PortalType type; // explore, connect, community, monitor
    private String piwik;
    private String twitterAccount;
    private List<PortalPage> pages;
    private List<PortalEntity> entities;

    public PortalResponse() {}

    public PortalResponse(Portal portal) {
        this.setId(portal.getId());
        this.setPid(portal.getPid());
        this.setName(portal.getName());
        this.setType(portal.getType());
        this.setPiwik(portal.getPiwik());
        this.setTwitterAccount(portal.getTwitterAccount());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        if(type == null) {
            return null;
        }
        return type.name();
    }

    public void setType(String type) {
        if(type == null) {
            this.type = null;
        } else {
            PortalType pType = PortalType.valueOf(type);
            this.type = pType;
        }
    }

    public String getPiwik() {
        return piwik;
    }

    public void setPiwik(String piwik) {
        this.piwik = piwik;
    }

    public String getTwitterAccount() {
        return twitterAccount;
    }

    public void setTwitterAccount(String twitterAccount) {
        this.twitterAccount = twitterAccount;
    }

    public List<PortalPage> getPages() { return pages; }

    public void setPages(List<PortalPage> pages) { this.pages = pages; }

    public List<PortalEntity> getEntities() { return entities; }

    public void setEntities(List<PortalEntity> entities) {
        this.entities = entities;
    }
}

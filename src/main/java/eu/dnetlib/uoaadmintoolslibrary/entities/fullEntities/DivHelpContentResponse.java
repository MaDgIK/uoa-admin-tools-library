package eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import org.springframework.data.annotation.Id;

import eu.dnetlib.uoaadmintoolslibrary.entities.DivHelpContent;

public class DivHelpContentResponse {
    @Id
    @JsonProperty("_id")
    private String id;

    private DivIdResponse divId;
    private Portal portal;
    private String content;
    private boolean isActive = true;

    public DivHelpContentResponse(DivHelpContent divHelpContent) {
        this.id = divHelpContent.getId();
        this.content = divHelpContent.getContent();
        this.isActive = divHelpContent.getIsActive();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DivIdResponse getDivId() {
        return divId;
    }

    public void setDivId(DivIdResponse divId) {
        this.divId = divId;
    }

    public Portal getPortal() {
        return portal;
    }

    public void setPortal(Portal portal) {
        this.portal = portal;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
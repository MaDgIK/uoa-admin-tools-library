package eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import eu.dnetlib.uoaadmintoolslibrary.entities.Entity;

public class PortalEntity {
    @Id
    @JsonProperty("_id")
    private String id;

    private String pid;
    private String name;
    private Boolean isEnabled;

    public PortalEntity() {}

    public PortalEntity(Entity entity) {
        this.setId(entity.getId());
        this.setPid(entity.getPid());
        this.setName(entity.getName());
        this.setIsEnabled(true);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) { this.pid = pid; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsEnabled() { return isEnabled; }

    public void setIsEnabled(Boolean isEnabled) { this.isEnabled = isEnabled; }
}


package eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.dnetlib.uoaadmintoolslibrary.entities.PortalType;
import org.springframework.data.annotation.Id;

import java.util.List;

import eu.dnetlib.uoaadmintoolslibrary.entities.Page;
import eu.dnetlib.uoaadmintoolslibrary.entities.DivId;

public class DivIdResponse {
    @Id
    @JsonProperty("_id")
    private String id;

    private String name;
    private List<Page> pages;
    private PortalType portalType; // explore, connect, community, monitor

//    private Boolean connect;
//    private Boolean communities;
//    private Boolean openaire;

    public DivIdResponse() {}

    public DivIdResponse(DivId divId) {
        setId(divId.getId());
        setName(divId.getName());
        setPortalType(divId.getPortalType());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Page> getPages() {
        return pages;
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

    public String getPortalType() {
        if(portalType == null) {
            return null;
        }
        return portalType.name();
    }

    public void setPortalType(String portalType) {
        if(portalType == null) {
            this.portalType = null;
        } else {
            PortalType pType = PortalType.valueOf(portalType);
            this.portalType = pType;
        }
    }
}

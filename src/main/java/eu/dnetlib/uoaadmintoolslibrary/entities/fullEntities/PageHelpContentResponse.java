package eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import org.springframework.data.annotation.Id;

import eu.dnetlib.uoaadmintoolslibrary.entities.Page;
import eu.dnetlib.uoaadmintoolslibrary.entities.PageHelpContent;

public class PageHelpContentResponse {

    @Id
    @JsonProperty("_id")
    private String id;

    private Page page;
    private Portal portal;
    private String placement;
    private int order;
    private String content;
    private boolean isActive = true;
    private boolean isPriorTo = false;

    public PageHelpContentResponse() {}

    public PageHelpContentResponse(PageHelpContent pageHelpContent) {
        this.id = pageHelpContent.getId();
        this.placement = pageHelpContent.getPlacement();
        this.order = pageHelpContent.getOrder();
        this.content = pageHelpContent.getContent();
        this.isActive = pageHelpContent.getIsActive();
        this.isPriorTo = pageHelpContent.getIsPriorTo();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Portal getPortal() {
        return portal;
    }

    public void setPortal(Portal portal) {
        this.portal = portal;
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsPriorTo() { return isPriorTo; }

    public void setIsPriorTo(boolean isPriorTo) { this.isPriorTo = isPriorTo; }
}
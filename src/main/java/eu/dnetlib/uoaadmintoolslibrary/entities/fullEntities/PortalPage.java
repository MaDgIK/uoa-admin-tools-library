package eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.dnetlib.uoaadmintoolslibrary.entities.PortalType;
import org.springframework.data.annotation.Id;

import java.util.List;

import eu.dnetlib.uoaadmintoolslibrary.entities.Entity;
import eu.dnetlib.uoaadmintoolslibrary.entities.Page;

public class PortalPage {

    @Id
    @JsonProperty("_id")
    private String id;

    private String route;
    private String name;
    private String type;
    private List<Entity> entities;
    private Boolean isEnabled;
    private PortalType portalType; // explore, connect, community, monitor
    private String portalPid; // e.g. covid-19, egi, ...

//    private Boolean connect;
//    private Boolean communities;
//    private Boolean openaire;

    // Posiitions where help texts are allowed for this page
    private Boolean top = false;
    private Boolean bottom = false;
    private Boolean left = false;
    private Boolean right = false;

    public PortalPage() {}

    public PortalPage(Page page) {
        this.setId(page.getId());
        this.setRoute(page.getRoute());
        this.setName(page.getName());
        this.setType(page.getType());
        this.setPortalType(page.getPortalType());
        this.setPortalPid(page.getPortalPid());
        this.setTop(page.getTop());
        this.setBottom(page.getBottom());
        this.setLeft(page.getLeft());
        this.setRight(page.getRight());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Entity> getEntities() { return entities; }

    public void setEntities(List<Entity> entities) { this.entities = entities; }

    public Boolean getIsEnabled() { return isEnabled; }

    public void setIsEnabled(Boolean isEnabled) { this.isEnabled = isEnabled; }

    public String getPortalType() {
        if(portalType == null) {
            return null;
        }
        return portalType.name();
    }

    public void setPortalType(String portalType) {
        if(portalType == null) {
            this.portalType = null;
        } else {
            PortalType pType = PortalType.valueOf(portalType);
            this.portalType = pType;
        }
    }

    public String getPortalPid() {
        return portalPid;
    }

    public void setPortalPid(String portalPid) {
        this.portalPid = portalPid;
    }

    public Boolean getTop() {
        return top;
    }

    public void setTop(Boolean top) {
        this.top = top;
    }

    public Boolean getBottom() {
        return bottom;
    }

    public void setBottom(Boolean bottom) {
        this.bottom = bottom;
    }

    public Boolean getLeft() {
        return left;
    }

    public void setLeft(Boolean left) {
        this.left = left;
    }

    public Boolean getRight() {
        return right;
    }

    public void setRight(Boolean right) {
        this.right = right;
    }
}

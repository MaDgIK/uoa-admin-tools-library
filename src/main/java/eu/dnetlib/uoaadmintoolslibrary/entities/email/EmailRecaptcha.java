package eu.dnetlib.uoaadmintoolslibrary.entities.email;

public class EmailRecaptcha {
    private Email email;
    private String recaptcha;

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public String getRecaptcha() {
        return recaptcha;
    }

    public void setRecaptcha(String recaptcha) {
        this.recaptcha = recaptcha;
    }
}

package eu.dnetlib.uoaadmintoolslibrary.entities;

public enum PortalType {
    explore, connect, community,
    monitor, funder, ri, project, organization,
    country, researcher, datasource,
    aggregator, eosc, publisher, journal
}

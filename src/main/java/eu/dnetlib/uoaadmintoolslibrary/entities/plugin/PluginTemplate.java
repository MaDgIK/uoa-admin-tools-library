package eu.dnetlib.uoaadmintoolslibrary.entities.plugin;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.dnetlib.uoaadmintoolslibrary.entities.PortalType;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PluginTemplate {

    @Id
    @JsonProperty("_id")
    private String id;

    private String name;
    private String description;
    private String code;
    private Boolean custom;
    private String image;
    private String page;
    private String placement;
    private Integer order;
    private PortalType portalType; // explore, connect, community, monitor
    private String plan; // e.g basic,  ultimate, etc for paid communities
    private Boolean defaultIsActive;
    private ArrayList<String> portalSpecific = new ArrayList<>();
    private Map<String,PluginAttributes> settings = new HashMap<>();
    private Object object;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public PortalType getPortalType() {
        return portalType;
    }

    public void setPortalType(PortalType portalType) {
        this.portalType = portalType;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public Map<String, PluginAttributes> getSettings() {
        return settings;
    }

    public void setSettings(Map<String, PluginAttributes> settings) {
        this.settings = settings;
    }


    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }

    public ArrayList<String> getPortalSpecific() {
        return portalSpecific;
    }

    public void setPortalSpecific(ArrayList<String> portalSpecific) {
        this.portalSpecific = portalSpecific;
    }

    public Boolean getDefaultIsActive() {
        return defaultIsActive;
    }

    public void setDefaultIsActive(Boolean defaultIsActive) {
        this.defaultIsActive = defaultIsActive;
    }

    public Boolean getCustom() {
        return custom;
    }

    public void setCustom(Boolean custom) {
        this.custom = custom;
    }

    @Override
    public String toString() {
        return "PluginTemplate{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", code='" + code + '\'' +
                ", custom=" + custom +
                ", image='" + image + '\'' +
                ", page='" + page + '\'' +
                ", placement='" + placement + '\'' +
                ", order=" + order +
                ", portalType=" + portalType +
                ", plan='" + plan + '\'' +
                ", defaultIsActive=" + defaultIsActive +
                ", portalSpecific=" + portalSpecific +
                ", settings=" + settings +
                ", object=" + object +
                '}';
    }
}

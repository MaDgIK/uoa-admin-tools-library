package eu.dnetlib.uoaadmintoolslibrary.entities.plugin;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.HashMap;
import java.util.Map;

public class Plugin {
    @Id
    @JsonProperty("_id")
    private String id;
    private String pid;
    private String templateCode;
    private String templateId;
    private String page;
    private String placement;
    private Integer order;
    private Boolean isActive = true;
    private Boolean isCustom = true;
    private Map<String,String> settingsValues = new HashMap<>();
    private Object object;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getTemplateCode() {


        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Map<String, String> getSettingsValues() {
        return settingsValues;
    }

    public void setSettingsValues(Map<String, String> settingsValues) {
        this.settingsValues = settingsValues;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public Boolean getCustom() {
        return isCustom;
    }

    public void setCustom(Boolean custom) {
        isCustom = custom;
    }

    @Override
    public String toString() {
        return "Plugin{" +
                "id='" + id + '\'' +
                ", pid='" + pid + '\'' +
                ", templateCode='" + templateCode + '\'' +
                ", templateId='" + templateId + '\'' +
                ", page='" + page + '\'' +
                ", placement='" + placement + '\'' +
                ", order=" + order +
                ", isActive=" + isActive +
                ", isCustom=" + isCustom +
                ", settingsValues=" + settingsValues +
                ", object=" + object +
                '}';
    }
}

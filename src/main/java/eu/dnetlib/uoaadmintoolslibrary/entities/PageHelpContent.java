package eu.dnetlib.uoaadmintoolslibrary.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

public class PageHelpContent {
    @Id
    @JsonProperty("_id")
    private String id;

    private String page;
    private String portal;
    private String placement;
    private int order;
    private String content;
    private boolean isActive = true;
    private boolean isPriorTo = false;

    public PageHelpContent() {}

    public PageHelpContent(String page, String portal, String placement, int order, String content, boolean isActive, boolean isPriorTo) {
        this.page = page;
        this.portal = portal;
        this.placement = placement;
        this.order = order;
        this.content = content;
        this.isActive = isActive;
        this.isPriorTo = isPriorTo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPortal() {
        return portal;
    }

    public void setPortal(String portal) {
        this.portal = portal;
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsPriorTo() { return isPriorTo; }

    public void setIsPriorTo(boolean isPriorTo) { this.isPriorTo = isPriorTo; }
}
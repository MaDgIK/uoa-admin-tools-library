package eu.dnetlib.uoaadmintoolslibrary.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.List;

public class Page {
    @Id
    @JsonProperty("_id")
    private String id;

    private String route;
    private String name;
    private String type;
    private List<String> entities;
    private PortalType portalType; // explore, connect, community, monitor
    private String portalPid; // e.g. covid-19, egi, ...

//    private Boolean connect;
//    private Boolean communities;
//    private Boolean openaire;

    // Positions where help texts are allowed for this page
    private Boolean top;
    private Boolean bottom;
    private Boolean left;
    private Boolean right;

    public Page() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getEntities() { return entities; }

    public void setEntities(List<String> entities) { this.entities = entities; }

//    public String getPortalType() { return portalType; }
//
//    public void setPortalType(String portalType) { this.portalType = portalType; }

    public String getPortalType() {
        if(portalType == null) {
            return null;
        }
        return portalType.name();
    }

    public void setPortalType(String portalType) {
        if(portalType == null) {
            this.portalType = null;
        } else {
            PortalType pType = PortalType.valueOf(portalType);
            this.portalType = pType;
        }
    }

    public String getPortalPid() {
        return portalPid;
    }

    public void setPortalPid(String portalPid) {
        this.portalPid = portalPid;
    }

    public Boolean getTop() {
        return top;
    }

    public void setTop(Boolean top) {
        this.top = top;
    }

    public Boolean getBottom() {
        return bottom;
    }

    public void setBottom(Boolean bottom) {
        this.bottom = bottom;
    }

    public Boolean getLeft() {
        return left;
    }

    public void setLeft(Boolean left) {
        this.left = left;
    }

    public Boolean getRight() {
        return right;
    }

    public void setRight(Boolean right) {
        this.right = right;
    }
}

package eu.dnetlib.uoaadmintoolslibrary.services;

import eu.dnetlib.uoaadmintoolslibrary.dao.PageHelpContentDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.Page;
import eu.dnetlib.uoaadmintoolslibrary.entities.PageHelpContent;
import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import eu.dnetlib.uoaadmintoolslibrary.entities.PortalType;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.PageHelpContentResponse;

import eu.dnetlib.uoaadmintoolslibrary.handlers.ContentNotFoundException;
import eu.dnetlib.uoaadmintoolslibrary.handlers.MismatchingContentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PageHelpContentService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private PageHelpContentDAO pageHelpContentDAO;

    @Autowired
    private PageService pageService;

    @Autowired
    private PortalService portalService;

    public List<PageHelpContentResponse> getPageHelpContents(String pid, String portalType, String page, String position, String active, String before) {
        List<PageHelpContent> pageHelpContents = null;

        Portal _portal = null;
        String portalId = null;
        if(pid != null) {
            _portal = portalService.getPortal(pid);
            if(_portal != null) {
                portalId = _portal.getId();
            }
        }

        if(pid != null && position != null && active != null && before != null) {
            pageHelpContents = pageHelpContentDAO.findByPortalAndPlacementAndIsActiveAndIsPriorToOrderByOrderAsc(portalId, position, Boolean.parseBoolean(active), Boolean.parseBoolean(before));
        } else if(pid != null && position != null && active != null) {
            pageHelpContents = pageHelpContentDAO.findByPortalAndPlacementAndIsActiveOrderByOrderAsc(portalId, position, Boolean.parseBoolean(active));
        } else if(pid != null && position != null && before != null) {
            pageHelpContents = pageHelpContentDAO.findByPortalAndPlacementAndIsPriorToOrderByOrderAsc(portalId, position, Boolean.parseBoolean(before));
        } else if(pid != null && active != null && before != null) {
            pageHelpContents = pageHelpContentDAO.findByPortalAndIsActiveAndIsPriorToOrderByPlacementAscOrderAsc(portalId, Boolean.parseBoolean(active), Boolean.parseBoolean(before));
        } else if(position != null && active != null && before != null) {
            pageHelpContents = pageHelpContentDAO.findByPlacementAndIsActiveAndIsPriorToOrderByOrderAsc(position, Boolean.parseBoolean(active), Boolean.parseBoolean(before));
        } else if(pid != null && position != null ) {
            pageHelpContents = pageHelpContentDAO.findByPortalAndPlacementOrderByOrderAsc(portalId, position);
        } else if(pid != null && active != null ) {
            pageHelpContents = pageHelpContentDAO.findByPortalAndIsActiveOrderByPlacementAscOrderAsc(portalId, Boolean.parseBoolean(active));
        } else if(pid != null && before != null) {
            pageHelpContents = pageHelpContentDAO.findByPortalAndIsPriorToOrderByPlacementAscOrderAsc(portalId, Boolean.parseBoolean(before));
        } else if(position != null && active != null) {
            pageHelpContents = pageHelpContentDAO.findByPlacementAndIsActiveOrderByOrderAsc(position, Boolean.parseBoolean(active));
        } else if(position != null && before != null) {
            pageHelpContents = pageHelpContentDAO.findByPlacementAndIsPriorToOrderByOrderAsc(position,  Boolean.parseBoolean(before));
        } else if(active != null && before != null) {
            pageHelpContents = pageHelpContentDAO.findByIsActiveAndIsPriorToOrderByPlacementAscOrderAsc(Boolean.parseBoolean(active), Boolean.parseBoolean(before));
        } else if(pid != null) {
            pageHelpContents = pageHelpContentDAO.findByPortalOrderByPlacementAscOrderAsc(portalId);
        } else if(position != null) {
            pageHelpContents = pageHelpContentDAO.findByPlacementOrderByOrderAsc(position);
        } else if(active != null) {
            pageHelpContents = pageHelpContentDAO.findByIsActiveOrderByPlacementAscOrderAsc(Boolean.parseBoolean(active));
        } else if(before != null) {
            pageHelpContents = pageHelpContentDAO.findByIsPriorToOrderByPlacementAscOrderAsc(Boolean.parseBoolean(before));
        } else {
            pageHelpContents = pageHelpContentDAO.findAllByOrderByPlacementAscOrderAsc();
        }

        List<PageHelpContentResponse> pageHelpContentResponses = new ArrayList<>();
        for (PageHelpContent pageHelpContent : pageHelpContents) {
            Page _page = pageService.getPage(pageHelpContent.getPage());
            if((page == null || page.equals(_page.getRoute())) && (portalType == null || portalType.equals(_page.getPortalType()))) {
                PageHelpContentResponse pageHelpContentResponse = new PageHelpContentResponse(pageHelpContent);

                pageHelpContentResponse.setPage(_page);
                pageHelpContentResponse.setPortal(portalService.getPortalById(pageHelpContent.getPortal()));
                pageHelpContentResponses.add(pageHelpContentResponse);
            }
        }
        return pageHelpContentResponses;
    }

    public List<PageHelpContent> getPageHelpContentsBasic(String pid, String portalType, String pageId) {
        List<PageHelpContent> pageHelpContents = null;

        Portal _portal = null;
        String portalId = null;
        if(pid != null) {
            _portal = portalService.getPortal(pid);
            portalService.checkPortalInfo(pid, portalType, _portal, pid, "pid");
            if(_portal != null) {
                portalId = _portal.getId();
            }
        }

        if(pid != null && pageId != null) {
            pageHelpContents = pageHelpContentDAO.findByPortalAndPageOrderByPlacementAscOrderAsc(portalId, pageId);
        } else if(pid != null) {
            pageHelpContents = pageHelpContentDAO.findByPortalOrderByPlacementAscOrderAsc(portalId);
        } else {
            pageHelpContents = pageHelpContentDAO.findAllByOrderByPlacementAscOrderAsc();
        }

        return pageHelpContents;
    }

    public void deleteAllPageHelpContents() {
        pageHelpContentDAO.deleteAll();
    }

//    public PageHelpContent insertPageHelpContent(PageHelpContent pageHelpContent) {
//        String portalId = portalService.getPortal(pageHelpContent.getPortal()).getId();
//        pageHelpContent.setPortal(portalId);
//        return pageHelpContentDAO.save(pageHelpContent);
//    }
//
//    public PageHelpContent updatePageHelpContent(PageHelpContent pageHelpContent) {
//        return pageHelpContentDAO.save(pageHelpContent);
//    }

    public PageHelpContent insertOrUpdatePageHelpContent(PageHelpContent pageHelpContent) {
        return pageHelpContentDAO.save(pageHelpContent);
    }

    public PageHelpContent getPageHelpContent(String id) {
        return pageHelpContentDAO.findById(id);
    }

    public List<String> togglePageHelpContent(List<String> pageHelpContents, String status, String pid, PortalType portalType) throws Exception {
        Portal portal = portalService.getPortal(pid);
        portalService.checkPortalInfo(pid, portalType.name(), portal, pid, "pid");

        for (String id: pageHelpContents) {
//            log.debug("Id of pageHelpContent: "+id);
            PageHelpContent pageHelpContent = pageHelpContentDAO.findById(id);
            if(pageHelpContent == null) {
                throw new ContentNotFoundException("Page help content with id: " + id + " not found");
            }
            if(!pageHelpContent.getPortal().equals(portal.getId())) {
                throw new MismatchingContentException("["+portalType+ " - "+ pid+"] Conflicting page help content: portal id: "+pageHelpContent.getPortal());
            }
            pageHelpContent.setIsActive(Boolean.parseBoolean(status));
            pageHelpContentDAO.save(pageHelpContent);
        }
        return pageHelpContents;
    }

    public void deletePageHelpContent(String id) {
        pageHelpContentDAO.delete(id);
    }

    public Boolean deletePageHelpContents(List<String> pageHelpContents, String pid, PortalType portalType) throws Exception {
        Portal portal = portalService.getPortal(pid);
        portalService.checkPortalInfo(pid, portalType.name(), portal, pid, "pid");

        for (String id: pageHelpContents) {
            PageHelpContent pageHelpContent = getPageHelpContent(id);
            if(pageHelpContent == null) {
                throw new ContentNotFoundException("Page help content with id: " + id + " not found");
            }
            if(!pageHelpContent.getPortal().equals(portal.getId())) {
                throw new MismatchingContentException("["+portalType+ " - "+ pid+"] Conflicting page help content: portal id: "+pageHelpContent.getPortal());
            }
            pageHelpContentDAO.delete(id);
        }
        return true;
    }

    public void addPageHelpContentsInPortal(String portalId, String portalType, String portalPid) {
        Page organizationsPage = pageService.getPageByPortalTypeAndRoute(portalType, "/organizations", portalPid);
        if(organizationsPage != null) {
            String organizations_page_content = "<div> <p>Here you can write more details about the organizations related to your community.</p> </div>";

            PageHelpContent organizations_pageHelpContent = new PageHelpContent(organizationsPage.getId(), portalId, "top", 1, organizations_page_content, false, false);
            //this.insertPageHelpContent(organizations_pageHelpContent);
            pageHelpContentDAO.save(organizations_pageHelpContent);
        }

        Page depositLearnHowPage = pageService.getPageByPortalTypeAndRoute(portalType, "/participate/deposit/learn-how", portalPid);
        if(depositLearnHowPage != null) {
            String depositLearnHow_page_content = "" +
                    "<div class=\"uk-width-3-5 uk-align-center\"> " +
                    " <div class=\"uk-text-bold\">How to comply with funder Open Access policies</div> " +
                    " <ul class=\"uk-list uk-list-bullet\"> " +
                    "   <li>Read the <a href=\"https://www.openaire.eu/how-to-comply-to-h2020-mandates-for-publications\" target=\"_blank\"> <span>OpenAIRE guide to learn how to comply with EC H2020 Open Access policy on publications>/span> <span class=\"custom-external custom-icon space\"> </span> </a></li> " +
                    "   <li>Read the <a href=\"https://www.openaire.eu/how-to-comply-to-h2020-mandates-for-data\" target=\"_blank\"> <span>OpenAIRE guide to learn how to comply with EC H2020 Open Access policy on research data</span> <span class=\"custom-external custom-icon space\"> </span></a></li> " +
                    "   <li>If you want to know about National Open Access policies, please check them out <a href=\"https://www.openaire.eu/frontpage/country-pages\" target=\"_blank\"><span>here</span> <span class=\"custom-external custom-icon space\"> </span></a></li> " +
                    "   <li>All OpenAIRE guides can be found <a href=\"https://www.openaire.eu/guides\" target=\"_blank\"><span>here</span> <span class=\"custom-external custom-icon space\"> </span></a></li> " +
                    " </ul> " +
                    "</div>";

            PageHelpContent depositLearnHow_pageHelpContent = new PageHelpContent(depositLearnHowPage.getId(), portalId, "bottom", 1, depositLearnHow_page_content, true, false);
            //this.insertPageHelpContent(depositLearnHow_pageHelpContent);
            pageHelpContentDAO.save(depositLearnHow_pageHelpContent);
        }
    }
}

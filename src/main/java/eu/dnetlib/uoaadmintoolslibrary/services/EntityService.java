package eu.dnetlib.uoaadmintoolslibrary.services;

import eu.dnetlib.uoaadmintoolslibrary.dao.EntityDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.Entity;
import eu.dnetlib.uoaadmintoolslibrary.entities.Page;
import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.PortalEntity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class EntityService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private EntityDAO entityDAO;

    @Autowired
    private PageService pageService;

    @Autowired
    private PortalService portalService;

    public List<Entity> getAllEntities() {
        return entityDAO.findAll();
    }

    public void deleteAllEntities() {
        entityDAO.deleteAll();
    }

    public Entity insertOrUpdateEntity(Entity entity) {
        return entityDAO.save(entity);
    }

    public Entity getEntity(String id) {
        return entityDAO.findById(id);
    }

    public Entity getEntityByPid(String pid) {
        return entityDAO.findByPid(pid);
    }

    public void deleteEntity(String id) {
        entityDAO.delete(id);
    }

    public PortalEntity updateEntity(PortalEntity portalEntity) {
        Entity entity = this.getEntityByPortalEntity(portalEntity);
        entityDAO.save(entity);

        return portalEntity;
    }

    public PortalEntity insertEntity(Entity entity) {
        Entity savedEntity = entityDAO.save(entity);
        PortalEntity portalEntity = new PortalEntity(savedEntity);

        // add entity in portals
        List<Portal> portals = portalService.getAllPortals();
        for( Portal portal : portals ) {
            Map<String, Boolean> entities = portal.getEntities();
            entities.put(entity.getId(), true);
            portal.setEntities(entities);
            portalService.insertOrUpdatePortal(portal);
        }

        return portalEntity;
    }

    private Entity getEntityByPortalEntity(PortalEntity portalEntity) {
        Entity entity = new Entity();
        entity.setId(portalEntity.getId());
        entity.setPid(portalEntity.getPid());
        entity.setName(portalEntity.getName());

        return entity;
    }

    public Boolean deleteEntities(List<String> entities) throws Exception {
        for (String id: entities) {
            // delete entity from portals
            List<Portal> portals = portalService.getAllPortals();
            for( Portal portal : portals ) {
                Map<String, Boolean> portalEntities = portal.getEntities();
                portalEntities.remove(id);
                portal.setEntities(portalEntities);
                portalService.insertOrUpdatePortal(portal);
            }

            // delete entity from pages
            List<Page> pages = pageService.getAllPages(null, null, null);
            for( Page page : pages ) {
                List<String> pageEntities = page.getEntities();
                Iterator<String> pageEntityIterator = pageEntities.iterator();
                while(pageEntityIterator.hasNext()) {
                    String pageEntity = pageEntityIterator.next();
                    if(pageEntity.equals(id)) {
                        pageEntityIterator.remove();
                        break;
                    }
                }
                page.setEntities(pageEntities);
                pageService.insertOrUpdatePage(page);
            }
            entityDAO.delete(id);
        }

        return true;
    }
}

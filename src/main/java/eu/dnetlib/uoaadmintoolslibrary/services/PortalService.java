package eu.dnetlib.uoaadmintoolslibrary.services;

import eu.dnetlib.uoaadmintoolslibrary.dao.PortalDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.DivId;
import eu.dnetlib.uoaadmintoolslibrary.entities.Entity;
import eu.dnetlib.uoaadmintoolslibrary.entities.Page;
import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.*;

import eu.dnetlib.uoaadmintoolslibrary.handlers.ContentNotFoundException;
import eu.dnetlib.uoaadmintoolslibrary.handlers.MismatchingContentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PortalService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private PortalDAO portalDAO;

    @Autowired
    private PageService pageService;

    @Autowired
    private EntityService entityService;

    @Autowired
    private DivIdService divIdService;

    @Autowired
    private PageHelpContentService pageHelpContentService;

    @Autowired
    private DivHelpContentService divHelpContentService;

    public List<Portal> getAllPortals() {
        List<Portal> portals = portalDAO.findAll();

        return portals;
    }

    public List<Portal> getAllPortalsByType(String type) {
        List<Portal> portals = portalDAO.findByType(type);

        return portals;
    }

    public List<PortalResponse> getPortalsFull(List<Portal> portals) {
        List<PortalResponse> portalsResponse = new ArrayList<>();
        for(Portal portal : portals) {
            PortalResponse portalResponse = new PortalResponse(portal);

            setEnabledPagesForPortalByType(portal, portalResponse);
            setEnabledEntitiesForPortalByType(portal, portalResponse);

            //Layout layout = layoutDAO.findById(portal.getLayout());
            //portalResponse.setLayout(layout);

            portalsResponse.add(portalResponse);
        }
        return portalsResponse;
    }

    public List<PortalResponse> getAllPortalsFull() {
        List<Portal> portals = this.getAllPortals();
        return this.getPortalsFull(portals);
    }

    public List<PortalResponse> getAllPortalsFullByType(String type) {
        List<Portal> portals = this.getAllPortalsByType(type);
        return this.getPortalsFull(portals);
    }

    private void setEnabledPagesForPortalByType(Portal portal, PortalResponse portalResponse) {
        List<PortalPage> pages = this.getPagesForPortalByType(portal.getPid(), null, null, null, null);
//        log.debug("PAGES number="+pages.size());
        Iterator<PortalPage> iteratorPages = pages.iterator();
        while(iteratorPages.hasNext()) {
            PortalPage page = iteratorPages.next();
            if(!page.getIsEnabled()) {
                iteratorPages.remove();
            }
        }
        portalResponse.setPages(pages);
//        log.debug("PAGES set");
    }

    private void setEnabledEntitiesForPortalByType(Portal portal, PortalResponse portalResponse) {
        List<PortalEntity> entities = this.getEntitiesForPortal(portal.getPid(), null);
//        log.debug("ENTITIES number="+entities.size());
        Iterator<PortalEntity> iteratorEntities = entities.iterator();
        while(iteratorEntities.hasNext()) {
            PortalEntity entity = iteratorEntities.next();
            if(!entity.getIsEnabled()) {
                iteratorEntities.remove();
            }
        }
        portalResponse.setEntities(entities);
//        log.debug("ENTITIES set");
    }

    public PortalResponse getPortalFull(String pid) {
        Portal portal = portalDAO.findByPid(pid);
        if(pid != null && portal == null) {
            return null;
        }
        PortalResponse portalResponse = new PortalResponse(portal);

        setEnabledPagesForPortalByType(portal, portalResponse);
        setEnabledEntitiesForPortalByType(portal, portalResponse);

        //Layout layout = layoutDAO.findById(portal.getLayout());
        //portalResponse.setLayout(layout);

        return portalResponse;
    }

    public PortalResponse updatePortal(Portal portal) {
        Portal com = portalDAO.findById(portal.getId());

//        Statistics statistics = statisticsDAO.findByPid(com.getPid());
//        statistics.setPid(portal.getPid());
//        statisticsDAO.save(statistics);
//
//        PortalSubscribers portalSubscribers =  communitySubscribersDAO.findByPid(com.getPid());
//        portalSubscribers.setPid(portal.getPid());
//        communitySubscribersDAO.save(portalSubscribers);

        com.setName(portal.getName());
        com.setPid(portal.getPid());
        com.setPiwik(portal.getPiwik());
        com.setTwitterAccount(portal.getTwitterAccount());

        portalDAO.save(com);
        PortalResponse portalResponse = this.getPortalFull(portal.getPid());

        return portalResponse;
    }

    public PortalResponse insertPortal(Portal portal) {
        List<PortalEntity> portalEntities = new ArrayList<>();
        List<PortalPage> portalPages = new ArrayList<>();
        Map<String, Boolean> entities = new HashMap<>();
        Map<String, Boolean> pages = new HashMap<>();

        for(Entity entity : entityService.getAllEntities()) {
            entities.put(entity.getId(), true);

            PortalEntity portalEntity = new PortalEntity(entity);
            portalEntity.setIsEnabled(true);
            portalEntities.add(portalEntity);
        }

        for(Page page : pageService.getPagesByPortalType(portal.getType(), portal.getPid())) {
            pages.put(page.getId(), true);

            PortalPage portalPage = new PortalPage(page);
            if(page.getRoute().equals("/curators") || page.getRoute().equals("/organizations")) {
                portalPage.setIsEnabled(false);
            } else {
                portalPage.setIsEnabled(true);
            }

            portalPages.add(portalPage);
        }

        portal.setEntities(entities);
        portal.setPages(pages);
//        Statistics statistics =  new Statistics(portal.getPid());
//        statisticsDAO.save(statistics);
//        PortalSubscribers portalSubscribers =  new PortalSubscribers(portal.getPid());
//        communitySubscribersDAO.save(portalSubscribers);
        Portal savedPortal = portalDAO.save(portal);
        PortalResponse portalResponse = this.getPortalFull(savedPortal.getPid());

//        log.debug("pid of saved portal: "+ savedPortal.getPid());

        String id = savedPortal.getId();

        divHelpContentService.addDivHelpContentsInPortal(id, savedPortal.getType());
        pageHelpContentService.addPageHelpContentsInPortal(id, savedPortal.getType(), savedPortal.getPid());

        return portalResponse;
    }

    private Portal getPortalByPortalResponse(PortalResponse portalResponse) {
        Portal portal = new Portal();
        portal.setId(portalResponse.getId());
        portal.setName(portalResponse.getName());

        List<PortalEntity> fullEntities = portalResponse.getEntities();
        Map<String, Boolean> entities = new HashMap<String, Boolean>();
        for(PortalEntity entity : fullEntities) {
            entities.put(entity.getId(), true);
        }
        for(Entity entity : entityService.getAllEntities()) {
            if(!entities.containsKey(entity.getId())) {
                entities.put(entity.getId(), false);
            }
        }
        portal.setEntities(entities);

        List<PortalPage> fullPages = portalResponse.getPages();
        Map<String, Boolean> pages = new HashMap<String, Boolean>();
        for(PortalPage page : fullPages) {
            pages.put(page.getId(), true);
        }
        for(Page page : pageService.getAllPages(null, null, null)) {
            if(!pages.containsKey(page.getId())) {
                pages.put(page.getId(), false);
            }
        }
        portal.setPages(pages);
//        Layout layout = portalResponse.getLayout();
//        portal.setLayout(layout.getId());

        return portal;
    }

    public Boolean deletePortals(List<String> portals) throws Exception {
        for (String id: portals) {
            Portal portal = portalDAO.findById(id);
            String pid = portal.getPid();

            // delete div contents related to this portal
            List<DivHelpContentResponse> divHelpContentResponses = divHelpContentService.getDivHelpContents(pid, null, null, null);
            for(DivHelpContentResponse divHelpContentResponse : divHelpContentResponses) {
                divHelpContentService.deleteDivHelpContent(divHelpContentResponse.getId());
            }

            // delete page contents related to this portal
            List<PageHelpContentResponse> pageHelpContentResponses = pageHelpContentService.getPageHelpContents(pid, null, null,null, null, null);
            for(PageHelpContentResponse pageHelpContentResponse : pageHelpContentResponses) {
                pageHelpContentService.deletePageHelpContent(pageHelpContentResponse.getId());
            }

//            Statistics stats = statisticsDAO.findByPid(pid);
//            if(stats != null) {
//                statisticsDAO.delete(stats.getId());
//            }
//
//            PortalSubscribers portalSubscribers = communitySubscribersDAO.findByPid(pid);
//            if(portalSubscribers != null) {
//                communitySubscribersDAO.delete(portalSubscribers.getId());
//            }
//
//            Layout layout = layoutDAO.findById(portal.getLayout());
//            if(layout != null) {
//                layoutDAO.delete(layout.getId());
//            }

            portalDAO.delete(id);
        }

        return true;
    }

    public String deletePortal(String id) {
        Portal portal = portalDAO.findById(id);
        String pid = portal.getPid();

        // delete div contents related to this portal
        List<DivHelpContentResponse> divHelpContentResponses = divHelpContentService.getDivHelpContents(pid, null, null, null);
        for(DivHelpContentResponse divHelpContentResponse : divHelpContentResponses) {
            divHelpContentService.deleteDivHelpContent(divHelpContentResponse.getId());
        }

        // delete page contents related to this portal
        List<PageHelpContentResponse> pageHelpContentResponses = pageHelpContentService.getPageHelpContents(pid, null, null,null, null, null);
        for(PageHelpContentResponse pageHelpContentResponse : pageHelpContentResponses) {
            pageHelpContentService.deletePageHelpContent(pageHelpContentResponse.getId());
        }

        // if no other portlas with the same type, delete pages and divIds for this portal type
        List<Portal> portalsWithSameType = portalDAO.findByType(portal.getType());
        if(portalsWithSameType == null || portalsWithSameType.size() == 1) {
            // delete pages for this portal type
            for(String pageId : portal.getPages().keySet()) {
                pageService.deletePage(pageId);
            }

            // delete divIds for this portal type
            List<DivId> divIds = divIdService.getDivIdsByPortalType(portal.getType());
            for(DivId divId : divIds) {
                divIdService.deleteDivId(divId.getId());
            }
        }

        portalDAO.delete(id);
        return pid;
    }

    public Portal insertOrUpdatePortal(Portal portal) {
        return portalDAO.save(portal);
    }

    public Portal getPortalById(String id) {
//        log.debug("ID: "+ id);
        return portalDAO.findById(id);
    }

    public Portal getPortal(String pid) {
//        log.debug("PID: "+ pid);
        return portalDAO.findByPid(pid);
    }

    public void deletePortalId(String id) {
        portalDAO.delete(id);
    }

    public List<PortalPage> getPagesForPortalByType(String pid, String page_type, String page_route, String div, String with_positions) {
        List<PortalPage> return_pages = new ArrayList<PortalPage>();
        Portal portal = portalDAO.findByPid(pid);
        if(portal == null) {
            throw new ContentNotFoundException("Portal with pid: " + pid + " not found");
        }
        Map<String, Boolean> pages = portal.getPages();

        if(pages != null) {
            for (Map.Entry<String, Boolean> page : pages.entrySet()) {
                if(div != null && div.equals("true")) {
                    List<DivId> divIds = divIdService.getDivIds(page.getKey(), null, null);
                    Iterator<DivId> divIdIterator = divIds.iterator();

                    while (divIdIterator.hasNext()) {
                        DivId divId = divIdIterator.next();
                        if(!portal.getType().equals(divId.getPortalType())) {
                            divIdIterator.remove();
                        }
                    }

                    if(divIds.isEmpty()) {
                        continue;
                    }
                }

                Page p = pageService.getPage(page.getKey());
                if(p == null) {
                    // EXCEPTION - MismatchingContent
                    throw new MismatchingContentException("get pages by portal type: Portal with id: "+portal.getId()+" has a page id: "+page.getKey()+" which does not match with any page");
                }

                if (with_positions != null) {
                    boolean at_least_one_position = Boolean.parseBoolean(with_positions);

                    if(at_least_one_position) {
                        if(!p.getTop() && !p.getBottom() && !p.getLeft() && !p.getRight()) {
                            continue;
                        }
                    } else {
                        if(p.getTop() || p.getBottom() || p.getLeft() || p.getRight()) {
                            continue;
                        }
                    }
                }

                if(portal.getType().equals(p.getPortalType())) {
                    if ((page_type == null && page_route == null) || (page_route == null && p.getType().equals(page_type))
                            || p.getRoute().equals(page_route)) {
                        PortalPage portalPage = new PortalPage(p);

                        List<Entity> entities = new ArrayList<>();
                        for (String entity : p.getEntities()) {
                            entities.add(entityService.getEntity(entity));
                        }
                        portalPage.setEntities(entities);
                        portalPage.setIsEnabled(page.getValue());

                        return_pages.add(portalPage);

                        if (page_route != null) {
                            break;
                        }
                    }
                }
            }
        }
        return_pages.sort(Comparator.comparing(PortalPage::getName));
        return return_pages;
    }

    public Portal insertOrUpdatePage(String id, PortalPage page) {
        Portal portal = portalDAO.findById(id);
        Map<String, Boolean> pages = portal.getPages();

        String name = page.getName();
        boolean isEnabled = page.getIsEnabled();

        pages.put(name, isEnabled);
        portal.setPages(pages);

        return portalDAO.save(portal);
    }

    public Portal togglePage(String pid, String portalType, List<String> pageIds, String status) throws Exception {
        Portal portal = portalDAO.findByPid(pid);
        checkPortalInfo(pid, portalType, portal, pid, "pid");
        Map<String, Boolean> pages = portal.getPages();

        for (String pageId: pageIds) {
            Page page = pageService.getPage(pageId);
            if(page == null) {
                throw new ContentNotFoundException("Page with id: " + pageId + " not found");
            }
            if(!page.getPortalType().equals(portalType)) {
                throw new MismatchingContentException("["+portalType+ " - "+ pid+"] Conflicting page type: "+page.getPortalType());
            }
//            log.debug("Toggle portal page: " + pageId + " of portal: " + pid + " to " + status);
            pages.put(pageId, Boolean.parseBoolean(status));
        }

        portal.setPages(pages);
        return portalDAO.save(portal);
    }

    public Portal toggleEntity(String pid, List<String> entityIds, String status) throws Exception {
        Portal portal = portalDAO.findByPid(pid);
        Map<String, Boolean> entities = portal.getEntities();
        Map<String, Boolean> pages = portal.getPages();

        for (String entityId: entityIds) {
//            log.debug("Toggle portal entity: " + entityId + " of portal: " + pid + " to " + status);

            entities.put(entityId, Boolean.parseBoolean(status));

            if(pages != null) {
                for (Map.Entry<String, Boolean> pageEntry : pages.entrySet()) {
                    Page page = pageService.getPage(pageEntry.getKey());
                    if (page.getEntities().contains(entityId) && page.getType().equals("search")) {
                        pages.put(pageEntry.getKey(), Boolean.parseBoolean(status));
                    }
                }
            }
        }

        portal.setEntities(entities);
        return portalDAO.save(portal);
    }

    public List<PortalEntity> getEntitiesForPortal(String pid, String entity) {
        List<PortalEntity> return_entities = new ArrayList<PortalEntity>();
        Map<String, Boolean> entities = portalDAO.findByPid(pid).getEntities();

        //log.debug("/portal/"+pid+"/entities -- entity: "+entity);
        if (entity != null) {
            Entity _entity = entityService.getEntityByPid(entity);
            String entityId = _entity.getId();
            PortalEntity portalEntity = new PortalEntity(_entity);
            portalEntity.setIsEnabled(entities.get(entityId));
            return_entities.add(portalEntity);
        } else {
            if(entities != null) {
                for (Map.Entry<String, Boolean> _entity : entities.entrySet()) {
                    PortalEntity portalEntity = new PortalEntity(entityService.getEntity(_entity.getKey()));
                    portalEntity.setIsEnabled(_entity.getValue());
                    return_entities.add(portalEntity);
                }
            }
        }
        return return_entities;
    }

//    @RequestMapping(value = "/community/{pid}/layout", method = RequestMethod.GET)
//    public Layout getLayoutForCommunity(@PathVariable(value = "pid") String pid) {
//        Portal portal = portalDAO.findByPid(pid);
//        if(portal.getLayout() != null) {
//            return layoutDAO.findById(portal.getLayout());
//        } else {
//            return null;
//        }
//    }

//    @RequestMapping(value = "/community/{pid}/layout", method = RequestMethod.POST)
//    public Layout updateLayoutForCommunity(@PathVariable(value = "pid") String pid, @RequestBody Layout layout) {
//        Portal portal = portalDAO.findByPid(pid);
//        if(portal.getLayout() != null) {
//            layout.setId(portal.getLayout());
//            layout = layoutDAO.save(layout);
//        } else {
//            layout = layoutDAO.save(layout);
//            portal.setLayout(layout.getId());
//            portalDAO.save(portal);
//        }
//        return layout;
//    }


    public Map<String, List<PageHelpContentResponse>> getPageHelpContentsByPosition(String pid, String page, String active) {
        Map<String, List<PageHelpContentResponse>> pageHelpContentResponses = new HashMap<>();

        List<PageHelpContentResponse> pageHelpContents = null;
        pageHelpContents = pageHelpContentService.getPageHelpContents(pid, null, page, null, active, null);

        pageHelpContentResponses.put("top", new ArrayList<>());
        pageHelpContentResponses.put("bottom", new ArrayList<>());
        pageHelpContentResponses.put("left", new ArrayList<>());
        pageHelpContentResponses.put("right", new ArrayList<>());

        for (PageHelpContentResponse pageHelpContentResponse : pageHelpContents) {
            pageHelpContentResponses.get(pageHelpContentResponse.getPlacement()).add(pageHelpContentResponse);
        }


        return pageHelpContentResponses;
    }

    public Map<String, List<DivHelpContentResponse>> getDivHelpContentsByPosition(String pid, String page, String active) {
        Map<String, List<DivHelpContentResponse>> divHelpContentResponses = new HashMap<>();

        List<DivHelpContentResponse> divHelpContents = null;
        divHelpContents = divHelpContentService.getDivHelpContents(pid, page, null, active);

        for (DivHelpContentResponse divHelpContentResponse : divHelpContents) {
            if(!divHelpContentResponses.containsKey(divHelpContentResponse.getDivId())) {
                divHelpContentResponses.put(divHelpContentResponse.getDivId().getName(), new ArrayList<>());
            }
            divHelpContentResponses.get(divHelpContentResponse.getDivId().getName()).add(divHelpContentResponse);
        }


        return divHelpContentResponses;
    }

    public void checkPortalInfo(String pid, String portalType, Portal portal, String portalId, String getBy) {
        if(portal == null) {
            if(portalId != null) {
                throw new ContentNotFoundException("Portal with "+getBy+": " + portalId + " not found");
            }
        }
        if(!portal.getType().equals(portalType)) {
            throw new MismatchingContentException("["+portalType+ " - "+ pid+"] Conflicting portal info: type: "+portal.getType());
        }
        if(!portal.getPid().equals(pid)) {
            throw new MismatchingContentException("["+portalType+ " - "+ pid+"] Conflicting portal info: pid: "+portal.getPid());
        }
    }
}

package eu.dnetlib.uoaadmintoolslibrary.services;

import eu.dnetlib.uoaadmintoolslibrary.dao.PageDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.DivId;
import eu.dnetlib.uoaadmintoolslibrary.entities.Entity;
import eu.dnetlib.uoaadmintoolslibrary.entities.Page;
import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.DivHelpContentResponse;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.PageHelpContentResponse;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.PortalPage;

import eu.dnetlib.uoaadmintoolslibrary.handlers.ContentNotFoundException;
import eu.dnetlib.uoaadmintoolslibrary.handlers.ForbiddenException;
import eu.dnetlib.uoaadmintoolslibrary.handlers.utils.RolesUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PageService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private RolesUtils rolesUtils;

    @Autowired
    private PageDAO pageDAO;

    @Autowired
    private PortalService portalService;

    @Autowired
    private PageHelpContentService pageHelpContentService;

    @Autowired
    private DivHelpContentService divHelpContentService;

    @Autowired
    private DivIdService divIdService;

    @Autowired
    private EntityService entityService;


    public List<PortalPage> getPagesFull(String pid, String page_route) {

        List<Page> pages = this.getAllPages(pid, page_route, null);
        if(pages == null) {
            return null;
        }

        List<PortalPage> portalPages = new ArrayList<>();
        for(Page page : pages) {
            PortalPage portalPage = new PortalPage(page);
            List<Entity> entities = new ArrayList<>();
            for(String entityId : page.getEntities()) {
                entities.add(entityService.getEntity(entityId));
            }
            portalPage.setEntities(entities);

            portalPages.add(portalPage);
        }

        return portalPages;
    }

    public List<Page> getAllPages(String pid, String page_route, String with_positions) {
        List<Page> pages = null;
        if (pid != null) {
            Portal portal = portalService.getPortal(pid);
            if(portal == null) {
                return null;
            }

            String portalType = portal.getType();

            if (page_route != null) {
                pages = new ArrayList<Page>();
                Page page = pageDAO.findByPortalTypeAndRouteAndPortalPidOrNull(portalType, page_route, pid);
                if(page != null) {
                    pages.add(page);
                }
            } else {
                pages = pageDAO.findByPortalTypeAndPortalPidOrNull(portalType, pid);
            }
        } else if (page_route != null) {
            pages = pageDAO.findByRoute(page_route);
        } else {
            pages = pageDAO.findAll();
        }

        if (with_positions != null) {
            if(pages == null) {
                return null;
            }

            boolean at_least_one_position = Boolean.parseBoolean(with_positions);

            Iterator<Page> iteratorPages = pages.iterator();
            while(iteratorPages.hasNext()) {
                Page page = iteratorPages.next();

                if(at_least_one_position) {
                    if(!page.getTop() && !page.getBottom() && !page.getLeft() && !page.getRight()) {
                        iteratorPages.remove();
                    }
                } else {
                    if(page.getTop() || page.getBottom() || page.getLeft() || page.getRight()) {
                        iteratorPages.remove();
                    }
                }
            }
        }
        if(pages != null) {
            pages.sort(Comparator.comparing(Page::getName));
        }
        return pages;
    }

    public List<Page> getPagesByPortalType(String portalType, String portalPid) {
        if (portalType == null) {
            return null;
        }
        return pageDAO.findByPortalTypeAndPortalPidOrNull(portalType, portalPid);
    }

    public Page getPageByPortalTypeAndRoute(String portalType, String page_route, String portalPid) {
        if (page_route == null || portalType == null) {
            return null;
        }
        return pageDAO.findByPortalTypeAndRouteAndPortalPidOrNull(portalType, page_route, portalPid);
    }

    public void deleteAllPages() {
        pageDAO.deleteAll();
    }

    public Page insertOrUpdatePage(Page page) {
        return pageDAO.save(page);
    }

    public PortalPage updatePage(PortalPage portalPage) {
        this.deletePageHelpContentsForPositionsIfDisabled(portalPage);
        Page page = this.getPageByPortalPage(portalPage);
        pageDAO.save(page);
        return portalPage;
    }

    public PortalPage insertPage(PortalPage portalPage) {
        // add page in portals
        List<Portal> portals = null;
        if(portalPage.getPortalPid() == null) {
            portals = portalService.getAllPortalsByType(portalPage.getPortalType());
        } else {
            Portal portal = portalService.getPortal(portalPage.getPortalPid());
            if(portal == null) {
                // EXCEPTION - MismatchingContent
                throw new ContentNotFoundException("Insert page: No portal found with pid: "+portalPage.getPortalPid());
            }
            portals = new ArrayList<>();
            portals.add(portal);
        }

        if(portals == null || portals.size() == 0) {
            throw new ContentNotFoundException("No portlas found with type: "+portalPage.getPortalType());
        }

        Page page = this.getPageByPortalPage(portalPage);
        Page savedPage = pageDAO.save(page);
        portalPage.setId(savedPage.getId());

        for( Portal portal : portals ) {
            Map<String, Boolean> pages = portal.getPages();
            pages.put(page.getId(), false);
            portal.setPages(pages);
            portalService.insertOrUpdatePortal(portal);
        }

        return portalPage;
    }

    private Page getPageByPortalPage(PortalPage portalPage) {
        Page page = new Page();
        page.setId(portalPage.getId());
        page.setRoute(portalPage.getRoute());
        page.setName(portalPage.getName());
        page.setType(portalPage.getType());
        page.setPortalType(portalPage.getPortalType());
        page.setPortalPid(portalPage.getPortalPid());
        page.setTop(portalPage.getTop());
        page.setBottom(portalPage.getBottom());
        page.setLeft(portalPage.getLeft());
        page.setRight(portalPage.getRight());

        List<Entity> fullEntities = portalPage.getEntities();
        List<String> entities = new ArrayList<String>();
        for(Entity entity : fullEntities) {
            entities.add(entity.getId());
        }
        page.setEntities(entities);

        return page;
    }

    private void deletePageHelpContentsForPositionsIfDisabled(PortalPage portalPage) {

        if(!portalPage.getTop()) {
            // delete page contents with position "top" related to this page from all portals
            List<PageHelpContentResponse> pageHelpContentResponses = pageHelpContentService.getPageHelpContents(null, portalPage.getPortalType(), portalPage.getRoute(), "top", null, null);
            for(PageHelpContentResponse pageHelpContentResponse : pageHelpContentResponses) {
                pageHelpContentService.deletePageHelpContent(pageHelpContentResponse.getId());
            }
        }

        if(!portalPage.getBottom()) {
            // delete page contents with position "bottom" related to this page from all portals
            List<PageHelpContentResponse> pageHelpContentResponses = pageHelpContentService.getPageHelpContents(null, portalPage.getPortalType(), portalPage.getRoute(), "bottom", null, null);
            for(PageHelpContentResponse pageHelpContentResponse : pageHelpContentResponses) {
                pageHelpContentService.deletePageHelpContent(pageHelpContentResponse.getId());
            }
        }

        if(!portalPage.getLeft()) {
            // delete page contents with position "left" related to this page from all portals
            List<PageHelpContentResponse> pageHelpContentResponses = pageHelpContentService.getPageHelpContents(null, portalPage.getPortalType(), portalPage.getRoute(), "left", null, null);
            for(PageHelpContentResponse pageHelpContentResponse : pageHelpContentResponses) {
                pageHelpContentService.deletePageHelpContent(pageHelpContentResponse.getId());
            }
        }

        if(!portalPage.getRight()) {
            // delete page contents with position "right" related to this page from all portals
            List<PageHelpContentResponse> pageHelpContentResponses = pageHelpContentService.getPageHelpContents(null, portalPage.getPortalType(), portalPage.getRoute(), "right", null, null);
            for(PageHelpContentResponse pageHelpContentResponse : pageHelpContentResponses) {
                pageHelpContentService.deletePageHelpContent(pageHelpContentResponse.getId());
            }
        }
    }

    public Boolean deletePages(List<String> pages) throws Exception {
        for (String id: pages) {
            Page page = pageDAO.findById(id);

            if(page == null) {
                throw new NullPointerException("Delete page: no page with id: "+id);
            }

            if(!rolesUtils.isPortalAdmin() || (
                    page.getPortalPid() != null && !rolesUtils.hasUpdateAuthority(page.getPortalType(), page.getPortalPid()))) {
                // EXCEPTION - Access denied
                throw new ForbiddenException("Delete page: You are not authorized to delete a page for "+page.getPortalType()+
                        (page.getPortalPid()!=null ? " : "+page.getPortalPid() : ""));
            }

            // delete divIds related only to this page from all portals, otherwise remove this page from divIds
            List<DivId> divIds = divIdService.getDivIds(id, null, null);
            for(DivId divId : divIds) {
                if(divId.getPages().size() == 1) {
                    // delete div contents related to this page from all portals
                    List<DivHelpContentResponse> divHelpContentResponses = divHelpContentService.getDivHelpContents(null, page.getRoute(), divId.getId(), null);
                    for (DivHelpContentResponse divHelpContentResponse : divHelpContentResponses) {
                        divHelpContentService.deleteDivHelpContent(divHelpContentResponse.getId());
                    }
                    divIdService.deleteDivId(divId.getId());

                } else {
                    List<String> divIdPages = divId.getPages();
                    divIdPages.remove(id);
                    divId.setPages(divIdPages);
                    divIdService.insertOrUpdateDivId(divId);
                }
            }


            // delete page contents related to this page from all portals
            List<PageHelpContentResponse> pageHelpContentResponses = pageHelpContentService.getPageHelpContents(null, page.getPortalType(), page.getRoute(), null, null, null);

            for(PageHelpContentResponse pageHelpContentResponse : pageHelpContentResponses) {
                pageHelpContentService.deletePageHelpContent(pageHelpContentResponse.getId());
            }

            // delete page from portals
            List<Portal> portals;
            if(page.getPortalPid() == null) {
                portals = portalService.getAllPortalsByType(page.getPortalType());
            } else {
                Portal portal = portalService.getPortal(page.getPortalPid());
                portals = new ArrayList<>();
                portals.add(portal);
            }

            if(portals != null) {
                for (Portal portal : portals) {
                    Map<String, Boolean> portalPages = portal.getPages();
                    portalPages.remove(id);
                    portal.setPages(portalPages);
                    portalService.insertOrUpdatePortal(portal);
                }
            }

            pageDAO.delete(id);
        }

        return true;
    }

    public Page getPage(String id) {
        return pageDAO.findById(id);
    }

    public void deletePage(String id) {
        pageDAO.delete(id);
    }

    public List<String> getPageEntities(String id) {
        return pageDAO.findById(id).getEntities();
    }

    public Page togglePageEntity(String id, String entityId, String status) throws Exception {
        log.debug("Toggle entity : "+entityId +" of page: "+id+" to "+status);
        Page page = pageDAO.findById(id);
        List<String> entities = page.getEntities();
        if(Boolean.parseBoolean(status) && !entities.contains(entityId)) {
            entities.add(entityId);
        } else if (!Boolean.parseBoolean(status)) {
            entities.remove(entityId);
        }
        page.setEntities(entities);
        return pageDAO.save(page);
    }

    public void updatePid(String old_pid, String new_pid, String portalType) {
        log.debug("page service: updatePid");
        List<Page> pages = pageDAO.findByPortalTypeAndPortalPidOrNull(portalType, old_pid);
        for(Page page : pages) {
            if (page != null) {
                page.setPortalPid(new_pid);
                pageDAO.save(page);
                log.debug("page "+page.getId()+" saved!");
            }
        }
    }
}

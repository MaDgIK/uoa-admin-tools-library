package eu.dnetlib.uoaadmintoolslibrary.services;

import eu.dnetlib.uoaadmintoolslibrary.dao.PluginDAO;
import eu.dnetlib.uoaadmintoolslibrary.dao.PluginTemplateDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.plugin.Plugin;
import eu.dnetlib.uoaadmintoolslibrary.entities.plugin.PluginTemplate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class PluginService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private PluginTemplateDAO pluginTemplateDAO;

    @Autowired
    private PluginDAO pluginDAO;

    public List<PluginTemplate> getPluginTemplates(String page, String name, String pid) {
        List<PluginTemplate> templates;
        if(page != null){
            templates = this.pluginTemplateDAO.findByPage(page);
        }else {
            templates = this.pluginTemplateDAO.findAll();
        }
        templates.sort(Comparator.comparingInt(PluginTemplate::getOrder));
        return templates;
    }

    public List<Plugin> getPlugins(String page, String name, String pid) {
        return this.pluginDAO.findAll();
    }
    public Plugin getPluginById(String id) {
        return  this.pluginDAO.findById(id);

    }
    public PluginTemplate getPluginTemplateById(String id) {
        return  this.pluginTemplateDAO.findById(id);

    }
    public List<Plugin> getPluginsByTemplateId(String id) {
        return this.pluginDAO.findByTemplateId(id);
    }

    public List<PluginTemplate> getPluginTemplatesByPage(String pageId) {
        ArrayList<String> pages = new ArrayList<String>();
        pages.add(pageId);
        List<PluginTemplate> templates = this.pluginTemplateDAO.findByPage(pageId);
        templates.sort(Comparator.comparingInt(PluginTemplate::getOrder));
        return templates;

    }
    public List<Plugin> getPluginsByPage(String pid, String pageId) {
        ArrayList<String> pages = new ArrayList<String>();
        pages.add(pageId);
        return  this.pluginDAO.findByPageAndPid(pageId,pid);

    }


    public PluginTemplate savePluginTemplate(PluginTemplate pluginTemplate) {
        return this.pluginTemplateDAO.save(pluginTemplate);
    }
    public Plugin savePlugin(Plugin plugin) {
        return this.pluginDAO.save(plugin);
    }

    public Plugin updatePluginStatus(String id, String status) {
        Plugin plugin = this.pluginDAO.findById(id);
        if(plugin != null) {
            plugin.setActive(Boolean.parseBoolean(status));
        }
        return this.pluginDAO.save(plugin);
    }


    public void deletePluginTemplate(String id) {
        pluginTemplateDAO.delete(id);
        List <Plugin> plugins = pluginDAO.findByTemplateId(id);
        for(Plugin p: plugins){
            pluginDAO.delete(p.getId());
        }

    }
    public void deletePlugin(String id) {
        pluginDAO.delete(id);
    }

}